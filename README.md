Boletador Validador de Posição de Ativos do Sistema Galgo
=========

Boletador Validador é um aplicativo desktop (JSwing) utilizado para boletagem - criação de um arquivo XML a a partir de uma planilha Excel - e validação de arquivo XML de Posição de Ativos versão 5.0 definido pela ANBIMA.