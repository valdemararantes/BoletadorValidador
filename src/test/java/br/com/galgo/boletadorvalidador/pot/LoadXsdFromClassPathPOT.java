package br.com.galgo.boletadorvalidador.pot;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Created by valdemar.arantes on 18/03/2015.
 */
public class LoadXsdFromClassPathPOT {
    private static final Logger log = LoggerFactory.getLogger(LoadXsdFromClassPathPOT.class);

    public static void main(String[] args) {
        new LoadXsdFromClassPathPOT().execute();
    }

    public void execute() {
        log.debug("******************* Início da execução **********************");
        try {
            final URL xsdURL = this.getClass().getClassLoader().getResource("wsdl/SchemaPosicaoAtivos.xsd");
            log.debug("xsdURL={}", xsdURL);
            final SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            final Schema schema = sf.newSchema(xsdURL);
            final Validator validator = schema.newValidator();
            final SAXSource source = new SAXSource(
                    new InputSource(new FileInputStream(new File("Erros\\Senior Solution\\2014-09-16\\ArqComNovasTags_novoXSD-de-Dados-Suplementares.xml"))));
            final List<String> errors = Lists.newArrayList();
            validator.setErrorHandler(new MyErrorHandler(errors));
            validator.validate(source);
            log.debug("Qtd. de erros encontrados: {}", errors.size());
        } catch (SAXException e) {
            log.error(null, e);
        } catch (FileNotFoundException e) {
            log.error(null, e);
        } catch (IOException e) {
            log.error(null, e);
        } finally {
            log.debug("******************* Fim da execução *******************");
        }
    }

    private static class MyErrorHandler extends DefaultHandler {

        private final List<String> errors;

        private MyErrorHandler(List<String> errors) {
            this.errors = errors;
        }

        @Override
        public void warning(SAXParseException e) throws SAXException {
            log.warn("Linha " + e.getLineNumber() + "; Coluna " + e.getColumnNumber()
                    + "; Descrição: " + e.getMessage());
            printInfo(e);
        }

        @Override
        public void error(SAXParseException e) throws SAXException {
            printInfo(e);
        }

        @Override
        public void fatalError(SAXParseException e) throws SAXException {
            printInfo(e);
        }

        private void printInfo(SAXParseException e) {
            errors.add("Linha " + e.getLineNumber() + "; Coluna " + e.getColumnNumber()
                    + "; Descrição: " + e.getMessage());
        }
    }
}
