/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.importers.XmlImporter;
import com.sistemagalgo.schemaposicaoativos.BsnsMsgComplexType;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.io.File;
import java.util.List;

/**
 *
 * @author valdemar.arantes
 */
public class ValoresReceber_016Test {

    private static final Logger log = LoggerFactory.getLogger(ValoresReceber_016Test.class);

    @Test
    public void validate_SEM_RECE() {
        log.debug("***************** Início ************************");
        final File xmlFile = new File(
                "Erros\\TOTVS\\2014-09-15\\"
                + "POSIÇÃO 5.0_ENVIO_20100105_20140819151403_8000_1_ATUALIZADO.XML");
        try {
            log.info("Arquivo XML: {}", xmlFile.getCanonicalPath());
            XmlImporter xmlImporter = new XmlImporter(xmlFile);
            GalgoAssBalStmtComplexType galgoAssBalStmt = (GalgoAssBalStmtComplexType) xmlImporter.importXml();
            List<BsnsMsgComplexType> bsnsMsgList = galgoAssBalStmt.getBsnsMsg();
            List<String> errors = new ValoresReceber_016().validate(bsnsMsgList.get(0).getDocument());
            if (!errors.isEmpty()) {
                int counter = 1;
                for (String error : errors) {
                    log.debug(counter + ": " + error);
                    counter++;
                }
            } else {
                log.debug("Nenhum erro encontrado");
            }

            Assertions.assertThat(errors).as("Lista de erros vazia").isEmpty();
        } catch (Exception e) {
            log.error(null, e);
            Assertions.fail("Exception: " + e.toString());
        } finally {
            log.debug("***************** Fim ************************");
        }
    }

    @Test
    public void validate_COM_RECE() {
        log.debug("***************** Início ************************");
        final File xmlFile = new File(
                "Erros\\Senior Solution\\2014-09-04"
                + "\\FD00000014742195_20111007_20140828164922Envio - Copy.xml");

        try {
            log.info("Arquivo XML: {}", xmlFile.getCanonicalPath());
            XmlImporter xmlImporter = new XmlImporter(xmlFile);
            GalgoAssBalStmtComplexType galgoAssBalStmt = (GalgoAssBalStmtComplexType) xmlImporter.importXml();
            List<BsnsMsgComplexType> bsnsMsgList = galgoAssBalStmt.getBsnsMsg();
            List<String> errors = new ValoresReceber_016().validate(bsnsMsgList.get(0).getDocument());
            if (!errors.isEmpty()) {
                int counter = 1;
                for (String error : errors) {
                    log.debug(counter + ": " + error);
                    counter++;
                }
            }

            Assertions.assertThat(errors).as("Lista de erros vazia").isEmpty();
        } catch (Exception e) {
            log.error(null, e);
            Assertions.fail("Exception: " + e.toString());
        } finally {
            log.debug("***************** Fim ************************");
        }
    }
}
