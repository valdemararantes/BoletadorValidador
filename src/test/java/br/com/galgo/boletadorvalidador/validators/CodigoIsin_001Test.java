package br.com.galgo.boletadorvalidador.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author valdemar.arantes
 */
public class CodigoIsin_001Test {

    private static final Logger log = LoggerFactory.getLogger(CodigoIsin_001Test.class);

    @Test
    public void verificaIso10383FalseTest() {
        try {
            String cod = "a";
            Assert.assertFalse(new CodigoIsin_001().verificaISO3166(cod));
        } catch (Exception e) {
            log.error(null, e);
        }
    }

    @Test
    public void verificaIso10383TrueTest() {
        try {
            String cod = "AF";
            Assert.assertTrue(new CodigoIsin_001().verificaISO3166(cod));
        } catch (Exception e) {
            log.error(null, e);
        }
    }
}
