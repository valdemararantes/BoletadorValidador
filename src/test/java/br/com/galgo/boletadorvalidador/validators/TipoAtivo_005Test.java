/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.importers.XmlImporter;
import com.sistemagalgo.schemaposicaoativos.BsnsMsgComplexType;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import jodd.bean.BeanUtil;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXB;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

/**
 * @author valdemar.arantes
 */
public class TipoAtivo_005Test {

    private static final Logger log = LoggerFactory.getLogger(TipoAtivo_005Test.class);
    private static final File xmlFile = new File("Erros/teste/testeVerde_1_ativo.xml");
    private static GalgoAssBalStmtComplexType galgoAssBalStmt;

    @BeforeClass
    public static void setUpClass() throws IOException {
        log.info("XML de teste: {}", xmlFile.getCanonicalPath());
        XmlImporter xmlImporter = new XmlImporter(xmlFile);
        galgoAssBalStmt = (GalgoAssBalStmtComplexType) xmlImporter.importXml();
    }

    @Test
    public void validateNoTagDefined() {
        try {
            List<BsnsMsgComplexType> bsnsMsgList = galgoAssBalStmt.getBsnsMsg();
            List<String> errors = new TipoAtivo_005().validate(bsnsMsgList.get(0).getDocument());
            if (!errors.isEmpty()) {
                int counter = 1;
                for (String error : errors) {
                    log.debug(counter + ": " + error);
                    counter++;
                }
            }

            Assertions.assertThat(errors).isNotEmpty();
            Assertions.assertThat(errors.size()).isEqualTo(1);
            Assertions.assertThat(errors.get(0)).contains(
                    "Não foi encontrada uma identificação de ativo com tipo \"TABELA NIVEL 1\"");
        } catch (Exception e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        }
    }

    @Test
    public void validateTagDefined() {
        try {
            List<BsnsMsgComplexType> bsnsMsgList = galgoAssBalStmt.getBsnsMsg();

            BeanUtil.setPropertyForced(galgoAssBalStmt,
                    "bsnsMsg[0].document.sctiesBalAcctgRpt.subAcctDtls[0].balForSubAcct[0]."
                            + "finInstrmId.othrId[0].id", "SHAR");
            OtherIdentification1 othrId = bsnsMsgList.get(0).getDocument().getSctiesBalAcctgRpt().
                    getSubAcctDtls().get(0).getBalForSubAcct().get(0).getFinInstrmId().
                    getOthrId().get(0);
            othrId.getTp().setCd(null);
            BeanUtil.setPropertyForced(othrId, "tp.prtry", "TABELA NIVEL 1");

            StringWriter xmlOut = new StringWriter();
            JAXB.marshal(othrId, xmlOut);
            log.debug("xmlOut=\n{}", xmlOut.toString());

            List<String> errors = new TipoAtivo_005().validate(bsnsMsgList.get(0).getDocument());
            if (!errors.isEmpty()) {
                int counter = 1;
                for (String error : errors) {
                    log.debug(counter + ": " + error);
                    counter++;
                }
            }

            Assertions.assertThat(errors).isEmpty();
        } catch (Exception e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        }
    }

    @Test
    public void validateTagDefinedWithInvalidCode() {
        try {
            List<BsnsMsgComplexType> bsnsMsgList = galgoAssBalStmt.getBsnsMsg();

            BeanUtil.setPropertyForced(galgoAssBalStmt,
                    "bsnsMsg[0].document.sctiesBalAcctgRpt.subAcctDtls[0].balForSubAcct[0]."
                            + "finInstrmId.othrId[0].id", "SHAR1");
            OtherIdentification1 othrId = bsnsMsgList.get(0).getDocument().getSctiesBalAcctgRpt().
                    getSubAcctDtls().get(0).getBalForSubAcct().get(0).getFinInstrmId().
                    getOthrId().get(0);
            othrId.getTp().setCd(null);
            BeanUtil.setPropertyForced(othrId, "tp.prtry", "TABELA NIVEL 1");

            StringWriter xmlOut = new StringWriter();
            JAXB.marshal(othrId, xmlOut);
            log.debug("xmlOut=\n{}", xmlOut.toString());

            List<String> errors = new TipoAtivo_005().validate(bsnsMsgList.get(0).getDocument());
            if (!errors.isEmpty()) {
                int counter = 1;
                for (String error : errors) {
                    log.debug(counter + ": " + error);
                    counter++;
                }
            }

            Assertions.assertThat(errors).isNotEmpty();
            Assertions.assertThat(errors.size()).isEqualTo(1);
            Assertions.assertThat(errors.get(0)).contains(
                    "Código SHAR1 não existe segundo a tabela de Tipos de Ativo ANBIMA");
        } catch (Exception e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        }
    }
}
