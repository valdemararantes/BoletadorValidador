/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.importers.XmlImporter;
import com.sistemagalgo.schemaposicaoativos.BsnsMsgComplexType;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceType12Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.TypeOfPrice11Code;
import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import jodd.bean.BeanUtil;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author valdemar.arantes
 */
public class CotasEmitir_013Test {

    private static final Logger log = LoggerFactory.getLogger(CotasEmitir_013Test.class);
    private static final String BEGIN_LOG
            = "************************ Início *****************************************";
    private static final String END_LOG
            = "************************ Fim ********************************************";

    @Test
    public void validateTOTVSFile() {
        log.debug(BEGIN_LOG);
        final File xmlFile = new File("Erros\\TOTVS\\2014-09-15\\"
                + "POSIÇÃO 5.0_ENVIO_20100105_20140819151403_8000_1_ATUALIZADO.XML");
        try {
            XmlImporter xmlImporter = new XmlImporter(xmlFile);
            GalgoAssBalStmtComplexType galgoAssBalStmt = (GalgoAssBalStmtComplexType) xmlImporter.
                    importXml();
            List<BsnsMsgComplexType> bsnsMsgList = galgoAssBalStmt.getBsnsMsg();
            List<String> errors = new CotasEmitir_013().validate(bsnsMsgList.get(0).getDocument());
            if (!errors.isEmpty()) {
                int counter = 1;
                for (String error : errors) {
                    log.debug(counter + ": " + error);
                    counter++;
                }
            } else {
                log.debug("Nenhum erro encontrado");
            }

            Assertions.assertThat(errors.isEmpty()).as("Lista de erros vazia").isTrue();
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.toString());
        }
        log.debug(END_LOG);
    }

    @Test
    public void validateValorFinancTotalNOK() {
        Document doc = new Document();
        try {
            BeanUtil.setPropertyForced(
                    doc, "sctiesBalAcctgRpt.balForAcct[0].balBrkdwn[0].subBalTp.cd", SecuritiesBalanceType12Code.PEND);

            //SubBalanceInformation6 o = doc.getSctiesBalAcctgRpt().getBalForAcct().get(0).getBalBrkdwn().get(0);
            SubBalanceInformation6 balBrkDwn = (SubBalanceInformation6) BeanUtil.getProperty(doc,
                    "sctiesBalAcctgRpt.balForAcct[0].balBrkdwn[0]");

            // Qtd. de Cotas (3)
            BeanUtil.setPropertyForced(balBrkDwn, "qty.qty.unit", BigDecimal.valueOf(3));

            // Valores Financeiros (totalizando 10 + 1 = 11)
            BeanUtil.setPropertyForced(balBrkDwn, "addtlBalBrkdwnDtls[0].qty.qty.faceAmt",
                    BigDecimal.TEN);
            BeanUtil.setPropertyForced(balBrkDwn, "addtlBalBrkdwnDtls[1].qty.qty.faceAmt",
                    BigDecimal.ONE);

            // Valor da Cota (1)
            AggregateBalanceInformation13 balForAcct = doc.getSctiesBalAcctgRpt().getBalForAcct().get(0);
            BeanUtil.setPropertyForced(balForAcct, "pricDtls[0].tp.cd", TypeOfPrice11Code.MRKT);
            BeanUtil.setPropertyForced(balForAcct, "pricDtls[0].val.amt.value", BigDecimal.ONE);

            List<String> errors = new CotasEmitir_013().validate(doc);
            if (!errors.isEmpty()) {
                int counter = 1;
                for (String error : errors) {
                    log.debug(counter + ": " + error);
                    counter++;
                }
            } else {
                log.debug("Nenhum erro encontrado");
            }

            Assertions.assertThat(errors).hasSize(1);
            Assertions.assertThat(errors.get(0)).contains(
                    "Valor total de Cotas a Emitir, representado pelo código PEND");
        } catch (Exception e) {
            Assertions.fail(e.toString());
        }
    }

    @Test
    public void validateValorFinancTotalOK() {
        Document doc = new Document();
        try {
            BeanUtil.setPropertyForced(
                    doc, "sctiesBalAcctgRpt.balForAcct[0].balBrkdwn[0].subBalTp.cd", SecuritiesBalanceType12Code.PEND);

            //SubBalanceInformation6 o = doc.getSctiesBalAcctgRpt().getBalForAcct().get(0).getBalBrkdwn().get(0);
            SubBalanceInformation6 balBrkDwn = (SubBalanceInformation6) BeanUtil.getProperty(doc,
                    "sctiesBalAcctgRpt.balForAcct[0].balBrkdwn[0]");

            // Qtd. de Cotas (3)
            BeanUtil.setPropertyForced(balBrkDwn, "qty.qty.unit", BigDecimal.valueOf(3));

            // Valores Financeiros (totalizando 10 + 2 = 12)
            BeanUtil.setPropertyForced(balBrkDwn, "addtlBalBrkdwnDtls[0].qty.qty.faceAmt",
                    BigDecimal.TEN);
            BeanUtil.setPropertyForced(balBrkDwn, "addtlBalBrkdwnDtls[1].qty.qty.faceAmt",
                    BigDecimal.valueOf(2));

            // Valor da Cota (4)
            AggregateBalanceInformation13 balForAcct = doc.getSctiesBalAcctgRpt().getBalForAcct().get(0);
            BeanUtil.setPropertyForced(balForAcct, "pricDtls[0].tp.cd", TypeOfPrice11Code.MRKT);
            BeanUtil.setPropertyForced(balForAcct, "pricDtls[0].val.amt.value", BigDecimal.valueOf(4));

            List<String> errors = new CotasEmitir_013().validate(doc);
            if (!errors.isEmpty()) {
                int counter = 1;
                for (String error : errors) {
                    log.debug(counter + ": " + error);
                    counter++;
                }
            } else {
                log.debug("Nenhum erro encontrado");
            }

            Assertions.assertThat(errors).isEmpty();
        } catch (Exception e) {
            Assertions.fail(e.toString());
        }
    }
}
