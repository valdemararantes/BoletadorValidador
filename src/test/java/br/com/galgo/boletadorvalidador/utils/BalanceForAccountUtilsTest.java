/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.utils;

import br.com.galgo.boletadorvalidador.importers.XmlImporter;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import java.io.File;
import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author valdemar.arantes
 */
public class BalanceForAccountUtilsTest {

    private static final Logger log = LoggerFactory.getLogger(BalanceForAccountUtilsTest.class);
    private static Document doc;

    @BeforeClass
    public static void beforeClass() {
        try {
            XmlImporter xmlImporter = new XmlImporter(new File("testeVerde.xml"));
            GalgoAssBalStmtComplexType galgoAssBalStmt = (GalgoAssBalStmtComplexType) xmlImporter.
                    importXml();
            doc = galgoAssBalStmt.getBsnsMsg().get(0).getDocument();
        } catch (Exception e) {
            log.error(null, e);
        }
    }

    @Test
    public void getFundoIdTest() {
        try {
            BalanceForAccountUtils balForAcctUtils = new BalanceForAccountUtils(doc.getSctiesBalAcctgRpt().getBalForAcct().get(0));
            List<BalanceForAccountUtils.Id> idList = balForAcctUtils.getIdList();
            for (BalanceForAccountUtils.Id id : idList) {
                log.debug(ToStringBuilder.reflectionToString(id));
            }
            Assertions.assertThat(idList.get(0).code).isEqualTo("CNPJ");
            Assertions.assertThat(idList.get(0).value).isEqualTo("71327334000112");
        } catch (Exception e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        }
    }
}
