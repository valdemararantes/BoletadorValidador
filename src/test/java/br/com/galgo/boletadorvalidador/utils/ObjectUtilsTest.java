/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.utils;

import com.google.common.collect.Lists;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author valdemar.arantes
 */
public class ObjectUtilsTest {

    private static final Logger log = LoggerFactory.getLogger(ObjectUtilsTest.class);

    private static class Pessoa {

        private String nome;
        private int idade;
        private final List<Pessoa> amigos = Lists.newArrayList();

        public Pessoa(String nome, int idade) {
            this.nome = nome;
            this.idade = idade;
        }

        public List<Pessoa> getAmigos() {
            return amigos;
        }

        public String getNome() {
            return nome;
        }

        public int getIdade() {
            return idade;
        }

        public void addAmigo(Pessoa amigo) {
            amigos.add(amigo);
        }

    }

    private static Pessoa neto;

    public ObjectUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        neto = new Pessoa("neto", 47);
        neto.addAmigo(new Pessoa("amigo 1", 25));
        neto.addAmigo(new Pessoa("amigo 2", 30));
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void getAsJsonString_PrettyTest() {
        String objStr = ObjectUtils.getAsJsonString(neto, true);
        log.debug("objStr={}", objStr);
        Assert.assertTrue(StringUtils.isNotBlank(objStr), "String gerada está vazia");
    }

    @Test
    public void getAsJsonString_NotPrettyTest() {
        String objStr = ObjectUtils.getAsJsonString(neto, false);
        log.debug("objStr={}", objStr);
        Assert.assertTrue(StringUtils.isNotBlank(objStr), "String gerada está vazia");
    }

    @Test
    public void getAsJsonString_NullTest() {
        String objStr = ObjectUtils.getAsJsonString(null, false);
        log.debug("objStr={}", objStr);
        Assert.assertNull(objStr);
    }
}
