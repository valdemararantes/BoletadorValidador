/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.utils;

import br.com.galgo.boletadorvalidador.importers.XmlImporter;
import com.galgo.utils.PropertyUtils;
import com.sistemagalgo.schemaposicaoativos.BsnsMsgComplexType;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import java.io.File;
import java.util.List;
import jodd.bean.BeanUtil;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.*;

/**
 *
 * @author valdemar.arantes
 */
public class BalanceForSubAccountUtilsTest {

    private static final Logger log = LoggerFactory.getLogger(BalanceForSubAccountUtilsTest.class);

    public BalanceForSubAccountUtilsTest() {
    }

    @DataProvider(name = "testGetIdAsString_Data")
    public Object[][] testGetIdAsString_Data() {
        return new Object[][]{
            {"Erros\\Senior Solution\\2014-09-04\\"
                + "FD00000014742195_20111007_20140828164922Envio - Copy.xml",
                "{\"isin\":\"BRSTNCLF1QR4\","
                + "\"othrId\":["
                + "{\"id\":\"210100\",\"tp\":{\"prtry\":\"SELC\"}},"
                + "{\"id\":\"GOVE\",\"tp\":{\"prtry\":\"TABELA NIVEL 1º\"}}]}"},
            {"Erros\\TOTVS\\2014-09-15\\"
                + "POSIÇÃO 5.0_ENVIO_20100105_20140819151403_8000_1_ATUALIZADO.XML",
                "{\"isin\":\"BRABNRC0V8G8\",\"othrId\":[]}"}
        };
    }

    @DataProvider(name = "testGetIdList_Data")
    public Object[][] testGetIdList_Data() {
        return new Object[][]{
            {"Erros\\Senior Solution\\2014-09-04\\"
                + "FD00000014742195_20111007_20140828164922Envio - Copy.xml",
                "[{\"code\":\"ISIN\",\"value\":\"BRSTNCLF1QR4\"},"
                + "{\"code\":\"SELC\",\"value\":\"210100\"},"
                + "{\"code\":\"TABELA NIVEL 1º\",\"value\":\"GOVE\"}]"},
            {"Erros\\TOTVS\\2014-09-15\\"
                + "POSIÇÃO 5.0_ENVIO_20100105_20140819151403_8000_1_ATUALIZADO.XML",
                "[{\"code\":\"ISIN\",\"value\":\"BRABNRC0V8G8\"}]"}
        };
    }

    @DataProvider(name = "testGetCodIdentAtivo_Data")
    public Object[][] testGetCodIdentAtivo_Data() {
        return new Object[][]{
            {"Erros\\Senior Solution\\2014-09-04\\"
                + "FD00000014742195_20111007_20140828164922Envio - Copy.xml",
                "GOVE"},
            {"Erros\\TOTVS\\2014-09-15\\"
                + "POSIÇÃO 5.0_ENVIO_20100105_20140819151403_8000_1_ATUALIZADO.XML",
                null}
        };
    }

    @Test(dataProvider = "testGetIdAsString_Data")
    public void testGetIdAsString(String fileName, String expectedId) {
        log.debug("************************ Início *****************************************");
        try {
            BsnsMsgComplexType bsnsMsg = loadBsnsMsg(fileName);
            AggregateBalanceInformation13 balForSubAcct
                    = (AggregateBalanceInformation13) PropertyUtils.getPropertyDN(bsnsMsg,
                            "document.sctiesBalAcctgRpt.subAcctDtls[0].balForSubAcct[0]");
            BalanceForSubAccountUtils balForSubAcctUtils = new BalanceForSubAccountUtils(
                    balForSubAcct);
            String result = balForSubAcctUtils.getIdAsString();
            Assertions.assertThat(result).isEqualTo(expectedId);
        } catch (Exception e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        } finally {
            log.debug("************************ Fim ********************************************");
        }
    }

    @Test(dataProvider = "testGetIdList_Data")
    public void testGetIdList(String fileName, String expectedId) {
        log.debug("************************ Início *****************************************");
        try {
            BsnsMsgComplexType bsnsMsg = loadBsnsMsg(fileName);
            List<AggregateBalanceInformation13> balForSubAcctList
                    = (List<AggregateBalanceInformation13>) PropertyUtils.
                    getProperty(bsnsMsg, "document.sctiesBalAcctgRpt.subAcctDtls[0].balForSubAcct");
            BalanceForSubAccountUtils instance = new BalanceForSubAccountUtils(
                    bsnsMsg.getDocument().getSctiesBalAcctgRpt().getSubAcctDtls().get(0).
                    getBalForSubAcct().get(0));
            List<BalanceForSubAccountUtils.Id> idList = instance.getIdList();
            String result = ObjectUtils.getAsJsonString(idList, false);
            Assertions.assertThat(result).isEqualTo(expectedId);
        } catch (Exception e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        } finally {
            log.debug("************************ Fim ********************************************");
        }
    }

    @Test(dataProvider = "testGetCodIdentAtivo_Data")
    public void testGetCodIdentAtivo(String fileName, String expected) {
        log.debug("************************ Início *****************************************");
        try {
            BsnsMsgComplexType bsnsMsg = loadBsnsMsg(fileName);
            AggregateBalanceInformation13 balForSubAcct
                    = (AggregateBalanceInformation13) PropertyUtils.
                    getProperty(bsnsMsg,
                            "document.sctiesBalAcctgRpt.subAcctDtls[0].balForSubAcct[0]");

            BalanceForSubAccountUtils balForSubAcctUtils = new BalanceForSubAccountUtils(
                    balForSubAcct);

            Assertions.assertThat(balForSubAcctUtils.getCodIdentAtivo()).isEqualTo(expected);
        } catch (Exception e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        } finally {
            log.debug("************************ Fim ********************************************");
        }
    }

    @Test
    public void setCodIdentAtivoTest() {
        log.debug("************************ Início *****************************************");
        try {
            Document doc = new Document();
            AggregateBalanceInformation13 balForSubAcct = new AggregateBalanceInformation13();
            BeanUtil.setPropertyForced(doc,
                    "sctiesBalAcctgRpt.subAcctDtls[0].balForSubAcct[0]", balForSubAcct);
            BalanceForSubAccountUtils utils = new BalanceForSubAccountUtils(balForSubAcct);
            boolean retFirstInvoke = utils.setCodIdentAtivo("AAAA");
            log.debug("doc=\n{}", ObjectUtils.getAsJsonString(doc, true));
            boolean retSecondInvoke = utils.setCodIdentAtivo("BBBB");
            Assertions.assertThat(retFirstInvoke).isTrue();
            Assert.assertEquals(utils.getCodIdentAtivo(), "AAAA");
            Assert.assertFalse(retSecondInvoke);
            Assert.assertEquals(utils.getCodIdentAtivo(), "AAAA");
        } catch (Exception e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        } finally {
            log.debug("************************ Fim ********************************************");
        }
    }

    private BsnsMsgComplexType loadBsnsMsg(String fileName) {
        log.debug("fileName={}", fileName);
        File xmlFile = new File(fileName);
        XmlImporter xmlImporter = new XmlImporter(xmlFile);
        GalgoAssBalStmtComplexType galgoAssBalStmt = (GalgoAssBalStmtComplexType) xmlImporter.
                importXml();
        return galgoAssBalStmt.getBsnsMsg().get(0);
    }

}
