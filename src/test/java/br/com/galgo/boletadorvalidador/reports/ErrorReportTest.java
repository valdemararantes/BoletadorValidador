/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.reports;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author valdemar.arantes
 */
public class ErrorReportTest {

    private static final Logger log = LoggerFactory.getLogger(ErrorReportTest.class);

    @BeforeClass
    public static void initClass() {
        log.debug("************* Classe ErrorReportTest *************");
    }

    @Test
    public void a() {
        log.debug("*************");
        File outFile = new File("outErrorReportTest_a.txt");
        ErrorReport errorReport = new ErrorReport(outFile);
        try {
            errorReport.addErrors("tit1", Arrays.asList("err1", "err2", "err3"));
            errorReport.addErrors("tit2", Arrays.asList("err4", "err5", "err6"));
            errorReport.print();

            Assertions.assertThat(outFile).isFile();
        } catch (IOException e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        }
    }
}
