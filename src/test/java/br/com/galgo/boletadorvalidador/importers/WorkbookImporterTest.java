/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.importers;

import br.com.galgo.boletadorvalidador.TestConstants;
import br.com.galgo.boletadorvalidador.reports.ErrorReport;
import br.com.galgo.boletadorvalidador.utils.GalgoAssBalStmtUtils;
import br.com.galgo.boletadorvalidador.validators.Validator;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

/**
 *
 * @author valdemar.arantes
 */
public class WorkbookImporterTest {

    private static final Logger log = LoggerFactory.getLogger(
            WorkbookImporterTest.class);

    @Test
    public void importXlsTest() {
        log.info(TestConstants.__TESTE_INI__);
        long startTime = System.nanoTime();
        try {

            GalgoAssBalStmtComplexType glg = new WorkbookImporter(new File("in_1.xls")).
                    importWorkbook();
            ObjectMapper mapper = new ObjectMapper();
            mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            StringWriter writer = new StringWriter();
            ObjectWriter writerWithDefaultPrettyPrinter = mapper.writerWithDefaultPrettyPrinter();
            writerWithDefaultPrettyPrinter.writeValue(writer, glg);
            String jsonString = writer.getBuffer().toString();
            log.info(jsonString);

//            GalgoAssBalStmtComplexType readValue = mapper.readValue(jsonString,
//                    GalgoAssBalStmtComplexType.class);
            Assertions.assertThat(glg.getGalgoHdr().getIdMsgSender()).isEqualTo("MSG1359996912215");
        } catch (Exception e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        } finally {
            long endTime = System.nanoTime();
            log.info("Fim ({} seg)", (endTime - startTime) / 1e9);
            log.info(TestConstants.__TESTE_FIM__);
        }
    }

    public void importXlsxTest() {
        log.info(TestConstants.__TESTE_INI__);
        long startTime = System.nanoTime();
        try {
            GalgoAssBalStmtComplexType importWorkbook = new WorkbookImporter(new File("in_1.xlsx")).
                    importWorkbook();
        } catch (Exception e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        } finally {
            long endTime = System.nanoTime();
            log.info("Fim ({} seg)", (endTime - startTime) / 1e9);
            log.info(TestConstants.__TESTE_FIM__);
        }
    }

    @Test
    public void buildXmlFromExcelAndValidate() {
        try {
            log.debug("**********************************");
            File xlsFileName = new File("../BoletadorImporter/boletador_template5.xls");
            File xmlFile = new File("../BoletadorImporter/boletador_template5.xml");
            File errFile = new File("../BoletadorImporter/boletador_template5.txt");
            InputStream wbInpStream = new FileInputStream(xlsFileName);
            Assertions.assertThat(xlsFileName).isFile();

            log.info("IMPORTAÇÃO");
            GalgoAssBalStmtComplexType glg = new br.com.galgo.boletador.importers.WorkbookImporter(
                    wbInpStream).importWorkbook();

            log.info("SALVANDO O XML");
            GalgoAssBalStmtUtils.marshall(glg, xmlFile);

            List<String> errors = Validator.validate(xmlFile, true);
            Map<String, List<String>> errorsMap = new LinkedHashMap<>();
            if (!errors.isEmpty()) {
                errorsMap.put("boletador_template5.xml", errors);
                ErrorReport errRpt = new ErrorReport(errFile);
                for (String section : errorsMap.keySet()) {
                    errRpt.addErrors(section, errorsMap.get(section));
                }
                errRpt.print();
            } else {
                log.info("Nenhum erro encontrado");
            }

        } catch (Exception e) {
            log.error(null, e);
            Assertions.fail(e.toString());
        }
    }

}
