/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.importers;

import com.google.common.collect.Maps;
import com.sistemagalgo.schemaposicaoativos.BsnsMsgComplexType;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import com.sistemagalgo.schemaposicaoativos.GalgoHdrComplexType;
import iso.std.iso._20022.tech.xsd.head_001_001.BusinessApplicationHeaderV01;
import iso.std.iso._20022.tech.xsd.head_001_001.GenericOrganisationIdentification1;
import iso.std.iso._20022.tech.xsd.head_001_001.OrganisationIdentification7;
import iso.std.iso._20022.tech.xsd.head_001_001.OrganisationIdentificationSchemeName1Choice;
import iso.std.iso._20022.tech.xsd.head_001_001.Party10Choice;
import iso.std.iso._20022.tech.xsd.head_001_001.Party9Choice;
import iso.std.iso._20022.tech.xsd.head_001_001.PartyIdentification42;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
class FundoImporter extends SheetImporter {

    private static final Logger log = LoggerFactory.getLogger(FundoImporter.class);
    private static final int HEADER_ROW_NUMBER = 1;
    private static final int DATA_ROW_NUMBER = HEADER_ROW_NUMBER + 1;
    private Map<String, Object> dataMap = Maps.newHashMap();

    public FundoImporter(GalgoAssBalStmtComplexType doc, Sheet sheet) {
        super(doc, sheet);
    }

    @Override
    public void importSheet() {
        log.info("Importando a aba {}", sheet.getSheetName());

        fillDataMap();
        fillObj();
    }

    /**
     * Preenche um Map a partir das linhas de Header e de dados
     */
    private void fillDataMap() {
        Row hdrRow = sheet.getRow(HEADER_ROW_NUMBER);
        short hdrLastCellNum = hdrRow.getLastCellNum();

        Row dataRow = sheet.getRow(DATA_ROW_NUMBER);
        short dataLastCellNum = dataRow.getLastCellNum();
        log.debug("hdrLastCellNum={}; dataLastCellNum={}", hdrLastCellNum, dataLastCellNum);

        for (int col = 0; col < hdrLastCellNum; col++) {
            Cell hdrCell = hdrRow.getCell(col, Row.RETURN_BLANK_AS_NULL);
            Cell dataCell = dataRow.getCell(col, Row.RETURN_BLANK_AS_NULL);
            String hdr = hdrCell.getStringCellValue();
            if (dataCell == null) {
                dataMap.put(hdr, null);
            } else {
                switch (dataCell.getCellType()) {
                    case Cell.CELL_TYPE_NUMERIC:
                        dataMap.put(hdr, dataCell.getNumericCellValue());
                        break;
                    case Cell.CELL_TYPE_STRING:
                        dataMap.put(hdr, dataCell.getStringCellValue());
                        break;
                    default:
                        log.warn("Dado da col={} com tipo={} não mapeado", col, dataCell.
                                getCellType());
                }
            }
        }

        log.debug("dataMap preenchido:\n{}", dataMap);
    }

    /**
     * Preenche os objetos JAX a partir do Map de dados
     */
    private void fillObj() {
        GalgoHdrComplexType glgHdr = new GalgoHdrComplexType();
        doc.setGalgoHdr(glgHdr);

        // Galgo Header
        glgHdr.setIdMsgSender(getStr("idMsgSender"));

        // Business Message
        List<BsnsMsgComplexType> bsnsMsgList = doc.getBsnsMsg();
        BsnsMsgComplexType bsnsMsg = new BsnsMsgComplexType();
        bsnsMsgList.add(bsnsMsg);

        // BAH
        bsnsMsg.setAppHdr(importBAH());

    }

    /**
     * Importa a sessão BAH
     *
     * @return
     */
    private BusinessApplicationHeaderV01 importBAH() {
        BusinessApplicationHeaderV01 appHdr = new BusinessApplicationHeaderV01();

        // FROM
        appHdr.setFr(new Party9Choice());
        PartyIdentification42 frOrgId = new PartyIdentification42();
        appHdr.getFr().setOrgId(frOrgId);
        frOrgId.setNm(getStr("Fr_OrgId_Name"));
        frOrgId.setId(new Party10Choice());
        frOrgId.getId().setOrgId(new OrganisationIdentification7());
        GenericOrganisationIdentification1 othrElem = new GenericOrganisationIdentification1();
        frOrgId.getId().getOrgId().getOthr().add(othrElem);
        othrElem.setId(getStr("Fr_OrgId_Id"));
        othrElem.setIssr(getStr("Fr_OrgId_Issuer"));
        othrElem.setSchmeNm(new OrganisationIdentificationSchemeName1Choice());
        othrElem.getSchmeNm().setCd(getStr("Fr_OrgId_Code"));

        // TO
        appHdr.setTo(new Party9Choice());
        appHdr.getTo().setOrgId(new PartyIdentification42());
        appHdr.getTo().getOrgId().setNm(getStr("To_OrgId_Name"));

        // Tags restantes
        appHdr.setBizMsgIdr(getStr("BizMsgIdr"));
        appHdr.setMsgDefIdr(getStr("MsgDefIdr"));
        appHdr.setBizSvc(getStr("BizSvc"));
        appHdr.setCreDt(null);

        return appHdr;
    }

    private String getStr(String key) {
        return dataMap.get(key).toString();
    }
}
