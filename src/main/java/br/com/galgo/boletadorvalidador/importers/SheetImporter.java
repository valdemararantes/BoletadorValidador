/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.importers;

import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import org.apache.poi.ss.usermodel.Sheet;

/**
 * Classe que deve ser implementada pelas classes importadoras de abas
 *
 * @author valdemar.arantes
 */
abstract class SheetImporter {

    final Sheet sheet;
    final GalgoAssBalStmtComplexType doc;

    /**
     *
     * @param doc Representação JAXB da Posição de Ativos a ser atualizada
     * @param sheet Aba a ser importada para atualizar o campo doc
     */
    public SheetImporter(GalgoAssBalStmtComplexType doc, Sheet sheet) {
        this.doc = doc;
        this.sheet = sheet;
    }

    /**
     * Importa os dados da aba, atualizando a representação DOM do XML
     */
    public abstract void importSheet();
}
