/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.importers;

import com.google.common.collect.Lists;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import java.lang.reflect.Constructor;
import java.util.List;
import org.apache.commons.lang.ClassUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe que coordena a importação das informações de cada aba. Os regras para a importação de cada
 * aba são as seguintes:
 *
 * <li>Todas as classes importadoras das abas devem estar devem possuir o pacote
 * <code>br.com.galgo.boletadorvalidador.importers</code></li>
 * <li>Todas as classes importadoras das abas devem implementar SheetImporter</li>
 * <li>Para cada aba é invocada uma classe responsável pela importação</li>
 * <li>O nome da classe é o mesmo nome da aba</li>
 * <li></li>
 *
 * @author valdemar.arantes
 */
class SheetsImporter {

    private static final Logger log = LoggerFactory.getLogger(SheetsImporter.class);
    private final Workbook wb;
    private GalgoAssBalStmtComplexType doc;

    SheetsImporter(Workbook wb) {
        this.wb = wb;

        // Cria objeto que representa a Posição de Ativos
        doc = new GalgoAssBalStmtComplexType();
    }

    public GalgoAssBalStmtComplexType importSheets() {

        int nSheets = wb.getNumberOfSheets();
        log.info("Planilha possui {} aba(s)", nSheets);
        if (nSheets == 0) {
            log.warn("Planilha sem abas. Retornando null");
            return null;
        }

        List<String> sheetNames = Lists.newArrayList();
        for (int i = 0; i < nSheets; i++) {
            sheetNames.add(wb.getSheetName(i));
        }
        log.debug("Nomes das abas: {}", sheetNames);

        // Importa cada aba
        for (int i = 0; i < nSheets; i++) {
            execSheetImporter(wb.getSheetAt(i));
        }

        return (doc.getBsnsMsg() != null && !doc.getBsnsMsg().isEmpty())? doc : null;
    }

    /**
     * Importa os dados de uma aba
     *
     * @param sheet
     */
    private void execSheetImporter(Sheet sheet) {
        try {
            log.debug("Carregando o ISheetImporter para a aba {}", sheet.getSheetName());
            String sheetImporterClassName = getImporterClassName(sheet);

            // Instancia a classe responsável pela aba
            Class<SheetImporter> sheetImporterClass = (Class<SheetImporter>) this.
                    getClass().getClassLoader().loadClass(
                    sheetImporterClassName);

            log.debug("SheetImporter encontrado: {}", sheetImporterClass.getName());
            Constructor<SheetImporter> constructor = sheetImporterClass.
                    getConstructor(GalgoAssBalStmtComplexType.class, Sheet.class);

            // Importa os dados da aba e atualiza o objeto doc
            log.debug("Executando a importação");
            SheetImporter sheetImporter = constructor.newInstance(doc, sheet);
            sheetImporter.importSheet();

        } catch (ClassNotFoundException e) {
            log.warn("Não existe uma classe importadora para a aba de nome {}", sheet.
                    getSheetName());
        } catch (Exception e) {
            log.error(null, e);
        }
    }

    private String getImporterClassName(Sheet sheet) {
        // Recupera o nome do pacote e o nome da classe importadora da aba
        String packageName = ClassUtils.getPackageName(this.getClass());
        String sheetImporterClassName = packageName + "." + sheet.getSheetName()
                + "Importer";
        return sheetImporterClassName;
    }
}
