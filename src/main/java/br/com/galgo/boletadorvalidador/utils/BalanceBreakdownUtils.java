/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.utils;

import com.galgo.utils.BeanUtils;
import com.galgo.utils.PropertyUtils;
import iso.std.iso._20022.tech.xsd.semt_003_001.AdditionalBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceType12Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceInformation6;
import java.math.BigDecimal;
import java.util.List;
import jodd.bean.BeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe utilitária para manipulação das informações relativas a BalanceBreakdown (segundo o manual da ANBIMA,
 * "Detalhes sobre o Patrimônio")
 *
 * @author valdemar.arantes
 */
public class BalanceBreakdownUtils {

    private static final Logger log = LoggerFactory.getLogger(BalanceBreakdownUtils.class);
    private final SubBalanceInformation6 balBrkdwn;

    /**
     *
     * @param balBrkdwn
     * @param parent
     */
    public BalanceBreakdownUtils(SubBalanceInformation6 balBrkdwn, AggregateBalanceInformation13 parent) {
        this.balBrkdwn = balBrkdwn;
    }

    /**
     *
     * @return Código do tipo de ativo
     */
    public SecuritiesBalanceType12Code getCodigoTipo() {
        return (SecuritiesBalanceType12Code) BeanUtil.getPropertySilently(balBrkdwn, "subBalTp.cd");
    }

    /**
     *
     * @return Quantidade de cotas
     */
    public BigDecimal getQtdCotas() {
        return (BigDecimal) BeanUtil.getPropertySilently(balBrkdwn, "qty.qty.unit");
    }

    /**
     *
     * @return Valor financeiro do ativo, obtido da seguinte forma:
     *  <li>No caso de cotas a receber (PEND) e cotas a pagar (PENR), o valor é declarado em
     *      balBrkdwn.qty.qty.faceAmt</li>
     *  <li>Nos demais casos, o valor é a soma dos valores declarados em
     *      balBrkdwn.addtlBalBrkdwnDtls[para cada um].qty.qty.faceAmt</li>
     */
    public BigDecimal getValorFinanceiro() {
        /*
        O que foi alterado é somente o tratamento para cotas a receber (PEND) e cotas a pagar (PENR) pois
        neste momento, o custodiante tem somente a informação do valor financeiro de cotas a receber/pagar,
        não há valor da cota ainda para se efetuar o calculo de quantidade de cotas convertidas. Por isso,
        o valor deve ser inserido em FaceAmount e não em Units. (e-mail da Ana Abidor de 22-12-2014)
         */
        if (SecuritiesBalanceType12Code.PENR.equals(getCodigoTipo())
        ||  SecuritiesBalanceType12Code.PEND.equals(getCodigoTipo())) {
            return (BigDecimal) BeanUtil.getPropertySilently(balBrkdwn, "qty.qty.faceAmt");
        }

        List<AdditionalBalanceInformation6> addtlBalBrkdwnDtlsLst = balBrkdwn.getAddtlBalBrkdwnDtls();
        if (addtlBalBrkdwnDtlsLst.isEmpty()) {
            return null;
        }

        BigDecimal valorFinancTotal = BigDecimal.ZERO;
        for (AdditionalBalanceInformation6 addtlBalBrkdwnDtls : addtlBalBrkdwnDtlsLst) {
            Object valorFinanc = BeanUtil.getPropertySilently(addtlBalBrkdwnDtls, "qty.qty.faceAmt");
            if (valorFinanc != null) {
                valorFinancTotal = valorFinancTotal.add((BigDecimal) valorFinanc);
            }
        }

        return valorFinancTotal;
    }
}
