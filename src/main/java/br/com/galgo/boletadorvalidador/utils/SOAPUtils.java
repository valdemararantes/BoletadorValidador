package br.com.galgo.boletadorvalidador.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.Detail;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.soap.SOAPFaultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class SOAPUtils {

	//@formatter:off
	public static final String SAMPLE_SOAP_MSG =
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            + "<SOAP-ENV:Envelope "
            +   "xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" "
            +   "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" "
            +   "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
            +   "<SOAP-ENV:Body>"
            +       "<add xmlns=\"http://ws.apache.org/counter/counter_port_type\">"
            +           "<value xmlns=\"\">15</value>"
            +       "</add>"
            +   "</SOAP-ENV:Body>"
            + "</SOAP-ENV:Envelope>";//@formatter:on

	public static final String SAMP_XML = "<requisicao></requisicao>";

	private static final Logger log = LoggerFactory.getLogger(SOAPUtils.class);

	private static final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

	static {
		factory.setNamespaceAware(true);
	}

	/**
	 * Retorna a descrição do erro (dsException) de um XML no formato
	 * <pre>
	 * &lt;?xml version="1.0" encoding="utf-8"?&gt;
	 * &lt;NS1:Envelope xmlns:NS1="http://schemas.xmlsoap.org/soap/envelope/"&gt;
	 *   &lt;NS1:Body&gt;
	 *     &lt;NS1:Fault&gt;
	 *       &lt;faultcode&gt;NS1:Server&lt;/faultcode&gt;
	 *       &lt;faultstring&gt;Código do Erro&lt;/faultstring&gt;
	 *       &lt;detail&gt;
	 *         &lt;NS2:MessageExceptionComplexType xmlns:NS2="http://www.stianbid.com.br/Common"&gt;
	 *           &lt;NS2:idException&gt;Código do erro&lt;/NS2:idException&gt;
	 *           &lt;NS2:dsException&gt;Uma descrição da mensagem&lt;/NS2:dsException&gt;
	 *         &lt;/NS2:MessageExceptionComplexType&gt;
	 *       &lt;/detail&gt;
	 *     &lt;/NS1:Fault&gt;
	 *   &lt;/NS1:Body&gt;
	 * &lt;/NS1:Envelope&gt;
	 * </pre>
	 * @param e
	 * @return
	 */
	public static String getFaultDescription(SOAPFaultException e) {
		if (e == null) {
			log.warn ("Tá louco, maluco? e == null !!!");
			return "Instância de SOAPFaultException nula!";
		}

		SOAPFault fault = e.getFault();
		Detail detail = fault.getDetail();
		if (detail == null) {
			log.warn("fault.detail == null");
			return XMLUtils.toPrettyString(fault);
		}

		Node msgComplexType = detail.getFirstChild();
		return msgComplexType.getFirstChild().getTextContent() + " - " + msgComplexType.getLastChild().getTextContent();
//		NodeList dsExceptionNodeList = e.getFault().getElementsByTagNameNS( "http://www.stianbid.com.br/Common", "dsException");
//		if (dsExceptionNodeList != null && dsExceptionNodeList.getLength() != 0) {
//			return dsExceptionNodeList.item(0).getTextContent();
//		} else {
//			Throwable cause = ExceptionUtils.getRootCause(e);
//			return cause.toString();// + ": " + cause.getMessage();
//		}
	}

	public static Node getRoot(SOAPMessage soapMsg) throws SOAPException, ParserConfigurationException,
			SAXException, IOException {
		return getRoot(soapMsg.getSOAPPart());
	}

	public static Node getRoot(SOAPPart soapPart) throws SOAPException, ParserConfigurationException,
			SAXException, IOException {
		Source source = soapPart.getContent();
		Node root = null;
		if (source instanceof DOMSource) {
			root = ((DOMSource) source).getNode();
		} else if (source instanceof SAXSource) {
			InputSource inSource = ((SAXSource) source).getInputSource();
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db = null;

			db = dbf.newDocumentBuilder();

			Document doc = db.parse(inSource);
			root = doc.getDocumentElement();
		}

		return root;
	}

	public static void main(String[] args) {
		try {
			//			Document sp = toSOAPPart(SAMPLE_SOAP_MSG);
			//			XMLUtils.dumpDocument(sp);
			//
			//			SOAPElement createElement = SOAPFactory.newInstance().createElement(sp.getDocumentElement());
			//			System.out.println("createElement=" + createElement);

			testMakeSOAPMessage();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Cria um SOAPMessage a partir de um XML DOM
	 * @param doc
	 * @return
	 * @throws SOAPException
	 */
	public static SOAPMessage makeSOAPMessage(Document doc) throws SOAPException {
		MessageFactory factory = MessageFactory.newInstance();
		SOAPMessage message = factory.createMessage();
		message.getSOAPPart().setContent(new DOMSource(doc));
		message.saveChanges();
		return message;
	}

	/**
	 * Cria um SoapMessage a partir de uma String XML
	 * @param xmlStr
	 * @return
	 * @throws SOAPException
	 */
	public static SOAPMessage makeSOAPMessage(String xmlStr) throws SOAPException {
		MessageFactory factory = MessageFactory.newInstance();
		SOAPMessage message = factory.createMessage();
		message.getSOAPPart().setContent(new StreamSource(new StringReader(xmlStr)));
		message.saveChanges();
		return message;
	}

	/**
	 *
	 * @param xml XML simples (filho do body)
	 * @return
	 * @throws SOAPException
	 */
	public static SOAPMessage makeSOAPMessageFromSimpleXML(Document xml) throws SOAPException {
		MessageFactory factory = MessageFactory.newInstance();
		SOAPMessage message = factory.createMessage();
		message.getSOAPBody().addDocument(xml);
		return message;
	}

	private static void testMakeSOAPMessage() {
		try {
			Document doc = toSOAPPart(SAMPLE_SOAP_MSG);
			log.debug("xml:\n\n" + XMLUtils.docToString(doc));
			SOAPMessage sm = makeSOAPMessage(doc);
			System.out.println(toString(sm));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String toPrettyString(SOAPMessage sm) {
		try {
			return XMLUtils.toPrettyString(getRoot(sm));
		} catch (Exception e) {
			throw new RuntimeException("Erro ao executar toPrettyString a partir do SOAPMessage", e);
		}
	}

	/**
	 * Convert an SOAP Envelope as a String to a org.w3c.dom.Document.
	 */
	public static Document toSOAPPart(String xml) throws Exception {
		InputStream in = new ByteArrayInputStream(xml.getBytes());
		DocumentBuilder builder = factory.newDocumentBuilder();
		return builder.parse(in);
	}

	public static String toString(SOAPMessage sm) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			sm.writeTo(baos);
		} catch (Exception e) {
			throw new RuntimeException("Erro ao executar toString a partir do SOAPMessage", e);
		}
		return new String(baos.toByteArray());
	}

}
