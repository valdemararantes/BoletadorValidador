/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.utils;

import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import java.util.List;

/**
 *
 * Classe retorna o identificador do Fundo ou Ativo
 *
 * @author joyce.oliveira
 */
public class Identificador {

    /**
     *
     * @param secaoId
     * @return O nome do identificador na posição 0 e o código na posição 1.
     *         Se secaiId = null, retorna null.
     */
    public String[] retornaIdentificador(SecurityIdentification14 secaoId) {
        if (secaoId == null) {
            return null;
        }

        String codigoIdentificador = null;
        String id[] = new String[2];

        //Se tiver ISIN, considera este identificador senão procura outro tipo
        String identificador = secaoId.getISIN();

        if (identificador != null) {
            codigoIdentificador = "ISIN";
        } //Se não tiver ISIN, procura Outro Identificador
        else {
            List<OtherIdentification1> othrIdLst = secaoId.getOthrId();

            for (OtherIdentification1 othrId : othrIdLst) {

                if ("SistemaGalgo".equals(othrId.getTp().getPrtry())) {
                    identificador = othrId.getId();
                    codigoIdentificador = "Código STI";
                    break;
                } else {
                    if ("CNPJ".equals(othrId.getTp().getCd())) {
                        codigoIdentificador = othrId.getTp().getCd();
                        identificador = othrId.getId();
                        break;
                    } else {
                        //Validação Sigla da Carteira
                    }
                }
                //TODO Incluir verificação de Sigla da Carteira

            }

        }

        id[0] = identificador;
        id[1] = codigoIdentificador;

        return id;
    }
}
