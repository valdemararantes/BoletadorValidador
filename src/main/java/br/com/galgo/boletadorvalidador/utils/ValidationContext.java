/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.utils;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author joyce.oliveira
 */
public class ValidationContext {

    private static Map<String, Object> ctx;


    public static void newContext(){
        ctx = new HashMap<>();
    }

   public void put(String key, Object value){
       ctx.put(key, value);
   }

   public Object get(String key){
       return ctx.containsKey(key);
   }

   public void destroi(){
       ctx.clear();
   }

}
