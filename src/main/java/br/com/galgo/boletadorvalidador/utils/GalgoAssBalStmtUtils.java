/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.utils;

import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import com.sistemagalgo.schemaposicaoativos.ObjectFactory;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author valdemar.arantes
 */
public class GalgoAssBalStmtUtils {

    /**
     * Transforma o objeto obj em XML e copia no arquivo toFile
     *
     * @param obj
     * @param toFile
     * @throws JAXBException
     */
    public static void marshall(GalgoAssBalStmtComplexType obj, File toFile) throws
            JAXBException {
        JAXBContext jc = JAXBContext.newInstance(GalgoAssBalStmtComplexType.class);
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        ObjectFactory objectFactory = new ObjectFactory();
        JAXBElement<GalgoAssBalStmtComplexType> galgoAssBalStmtElement = objectFactory.
                createGalgoAssBalStmt(obj);
        marshaller.marshal(galgoAssBalStmtElement, toFile);
    }
}
