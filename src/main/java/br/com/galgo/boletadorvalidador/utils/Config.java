package br.com.galgo.boletadorvalidador.utils;

import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Properties;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Carrega as configuracoes do aplicativo
 *
 * @author valdemar.arantes
 *
 */
public class Config {

    public static final String NS_SCH_FUNDO = "http://www.stianbid.com.br/SchemaFundo";
    public static final String NS_SCH_COMMON = "http://www.stianbid.com.br/Common";

    private static final String CONFIG_FILE = "/config.properties";
    private static final Logger log = LoggerFactory.getLogger(Config.class);
    private static Properties props;
    private static URL urlFileToWatch;

    static {
        init_();
    }

    /**
     * @return true se init foi OK
     */
    private static void init_() {
        log.debug("Carregando as configuracoes do aplicativo");

        // Define o formato do ToString das classes do tipo ValueObject
        ToStringBuilder.setDefaultStyle(ToStringStyle.MULTI_LINE_STYLE);

        log.debug("Config.class.getResource(\""+ CONFIG_FILE + "\")={}", Config.class.getResource(CONFIG_FILE));
        InputStream is = Config.class.getResourceAsStream(CONFIG_FILE);
        props = new Properties();
        try {
            props.load(is);
            is.close();
            log.debug("config: " + props.toString());
        } catch (Exception e) {
            log.error(null, e);
            throw new RuntimeException(e);
        }
    }

    public static Properties getProperties() {
        return props;
    }

    public static String getString(String key) {
        return props.getProperty(key);
    }

    public static String getProperty(String propName) {
        return props.getProperty(propName);
    }

    /**
     * Obtém o valor de uma propriedade na forma de uma array de Strings, qebradas nos espaços
     * em branco. Retorna nulo se a propriedade não estiver definida.
     * @param propName
     * @return
     */
    public static String[] getStringList(String propName) {
        String keyValue = props.getProperty(propName);
        return keyValue == null ? null : StringUtils.split(keyValue);
    }
}
