/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.galgo.boletadorvalidador.utils;

import java.io.StringWriter;
import javax.xml.bind.JAXB;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class ObjectUtils {

    private static final Logger log = LoggerFactory.getLogger(ObjectUtils.class);

    /**
     *
     * @param obj
     * @param pretty Indica se deve formatar (true) ou não
     * @return Retorna um string JSON com os dados do objeto obj. Se obj nulo, retorna nulo.
     */
    public static String getAsJsonString(Object obj, boolean pretty) {
        if (obj == null) {
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        StringWriter writer = new StringWriter();
        ObjectWriter writerWithDefaultPrettyPrinter =
                pretty ? mapper.writerWithDefaultPrettyPrinter() : mapper.writer();
        try {
            writerWithDefaultPrettyPrinter.writeValue(writer, obj);
            String jsonString = writer.getBuffer().toString();
            return jsonString;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * @param obj
     * @return Retorna uma string XML com os dados do objeto obj. Se obj nulo, retorna nulo.
     */
    public static String getAsXmlString(Object obj) {
        if (obj == null) {
            return null;
        }

        StringWriter w = new StringWriter();
        JAXB.marshal(obj, w);
        return w.toString();
    }
}
