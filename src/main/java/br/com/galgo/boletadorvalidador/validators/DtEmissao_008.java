/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Config;
import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.List;

/**
 * Data da emissão deve ser:
 * Anterior ou igual à data do arquivo;
 * Anterior ou igual à data de vencimento do ativo;
 * <p/>
 * ESTA SUBSEÇÃO É OBRIGATÓRIA PARA
 * - TÍTULOS
 * - SWAP
 * - DEBÊNTURES
 * - TERMO
 * - EMPRÉSTIMO DE AÇÕES (INDICAR A DATA DA OPERAÇÃO DE EMPRÉSTIMO)
 * - OPERAÇÕES COMPROMISSADAS (INDICAR DATA DA OPERAÇÃO DO COMPROMISSO)
 *
 * @author joyce.oliveira
 */

@ValidatorType
class DtEmissao_008 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(DtEmissao_008.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste de Data da Emissão");

        errors = Lists.newArrayList();

        boolean verificaDataEmissao = false;
        String tipoAtivo = null;
        String otherId_Id = null;

        // Lista dos tipos de ativos que obriga a utilização da data de emissão
        List<String> listaTpAtivosDtEmissaoObrigatorio = Lists.newArrayList(Config.getStringList(
                "tp_ativo_dt_emissao_obrigatorio"));

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();
        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();

            //Verificar se tipo do ativo obriga Informar Campo Interest Rate
            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
                List<OtherIdentification1> finInstrmId = listaAtivos.getFinInstrmId().getOthrId();

                //Capturando a identificação do ativo
                Identificador identifica = new Identificador();
                SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
                String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                verificaDataEmissao = false;
                try {

                    for (OtherIdentification1 otherId : finInstrmId) {
                        tipoAtivo = otherId.getTp().getPrtry();
                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        if (tipoAtivo.equals(Config.getString("tabela_identicacao_ativos"))) {
                            otherId_Id = otherId.getId();
                            //log.debug("Tipo CVM do ativo = {}", otherId_Id);
                            //Se for código 147,193,42,43, 196 ou 75 deve verificar Data Emissão
                            if (listaTpAtivosDtEmissaoObrigatorio.contains(otherId_Id)) {
                                verificaDataEmissao = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }

                if (verificaDataEmissao) {
                    List<SubAccountIdentification16> subAcctDtlsLista = doc.getSctiesBalAcctgRpt().
                            getSubAcctDtls();
                    String dataEmissaoAtivo;
                    XMLGregorianCalendar dataArquivo;
                    String dataVencimentoAtivo;
                    int comparacao;

                    /**
                     * Verificar Data do Arquivo
                     */
                    dataArquivo = doc.getSctiesBalAcctgRpt().getStmtGnlDtls().getStmtDtTm().getDt();
                    //log.debug("Data arquivo -> {}", dataArquivo);

                    /**
                     * Verificar a Data de Emissão do Ativo e Data de Vencimento
                     */
                    XMLGregorianCalendar dtEmissao = listaAtivos.getFinInstrmAttrbts().
                            getIsseDt();
                    dataEmissaoAtivo = dtEmissao.toString();
                    //log.debug("Data Emissão -> {}", dataEmissaoAtivo);
                    XMLGregorianCalendar dtVcmtAtivo = listaAtivos.getFinInstrmAttrbts().
                            getMtrtyDt();
                    dataVencimentoAtivo = dtVcmtAtivo.toString();

                    String identificacaoAtivo = listaAtivos.getFinInstrmId().getISIN();

                    //log.debug("Data Vencimento -> {}", dataVencimentoAtivo);
                    try {
                        comparacao = dtEmissao.compare(dtVcmtAtivo);

                        if (comparacao == 1) {
                            errors.
                                    add("Data de Emissão do Ativo deve ser anterior ou igual a Data de "
                                            + "Vencimento. Foi informada Data de Emissao: " + dataEmissaoAtivo
                                            + " e Data de Vencimento: " + dtVcmtAtivo + ". Verificar ativo de código "
                                            + id[1] + " igual a " + id[0]
                                            + ".  <BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                            + "<BalForSubAcct><FinInstrmAttrbts><IsseDt>");
                        }
                    } catch (NullPointerException e) {
                        errors.
                                add("Data de Emissão do Ativo deve ser anterior ou igual a Data de "
                                        + "Vencimento. Foi informada Data de Emissao: " + dataEmissaoAtivo
                                        + " e Data de Vencimento: " + dtVcmtAtivo + ". Verificar ativo de código "
                                        + id[1] + " igual a " + id[0] + ". <BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                        + "<BalForSubAcct><FinInstrmAttrbts><IsseDt>");
                    }


                    //Comparando se a data de emissão é anterior ou igual a data Vencimento

                    //Comparando se a data de emissão é anterior ou igual a Data do Arquivo
                    try {
                        comparacao = dtEmissao.compare(dataArquivo);

                        if (comparacao == 1) {
                            errors.
                                    add("Data de Emissão do Ativo deve ser anterior ou igual a Data do "
                                            + "Arquivo. Foi informada Data de Emissao: " + dataEmissaoAtivo
                                            + " e Data do Arquivo: " + dataArquivo + ". Verificar ativo de código "
                                            + id[1] + " igual a " + id[0]
                                            + ".  <BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                            + "<BalForSubAcct><FinInstrmAttrbts><IsseDt>");
                        }

                    } catch (Exception e) {
                        errors.
                                add("Data de Emissão do Ativo deve ser anterior ou igual a Data do "
                                        + "Arquivo. Foi informada Data de Emissao: " + dataEmissaoAtivo
                                        + " e Data do Arquivo: " + dataArquivo + ". Verificar ativo de código " + id[1]
                                        + " igual a " + id[0] + ".  <BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                        + "<BalForSubAcct><FinInstrmAttrbts><IsseDt>");
                    }
                }
            }
        }
        return errors;
    }
}