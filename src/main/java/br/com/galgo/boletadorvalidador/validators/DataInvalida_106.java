/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *O erro PA.0076 ocorre quando a tag “/SctiesBalAcctgRpt /StmtGnlDtls/StmtDtTm/DtTm”
 * for enviada. A Data da Informação não deve conter horas/minutos
 *
 * @author joyce.oliveira
 */
@GalgoValidatorType
class DataInvalida_106 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            DataInvalida_106.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Data Invalida");

        errors = Lists.newArrayList();

            XMLGregorianCalendar dataArquivo = doc.getSctiesBalAcctgRpt().getStmtGnlDtls().
                    getStmtDtTm().getDt();

            if(dataArquivo == null){
                errors.add("Validação do Sistema Galgo : PA.0076 - Foi enviada a tag "
                        + "<BsnsMsg><SctiesBalAcctgRpt><StmtGnlDtls><StmtDtTm>"
                        + "<DtTm>. A Data da Informação não deve conter horas/minutos. "
                        + "<BsnsMsg><SctiesBalAcctgRpt><StmtGnlDtls><StmtDtTm><Dt>");
            }

        return errors;
}
}
