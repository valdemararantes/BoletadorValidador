/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * ISIN + Identificação Proprietária são obrigatórios para títulos publicos
 *
 * @author joyce.oliveira
 */
@ValidatorType
class ISINObrigatorio_041 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            ISINObrigatorio_041.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste ISIN Obrigatório");

        errors = Lists.newArrayList();

        //Conferir se foi informada o cóigo ISIN

        List<String> listaCodigosCVM = new ArrayList<>();
        Identificador identifica = new Identificador();

        //Códigos CVM dos tipos de ativos que devem informar ISIN
        listaCodigosCVM.add("147");
        listaCodigosCVM.add("37");
        listaCodigosCVM.add("39");
        listaCodigosCVM.add("40");
        listaCodigosCVM.add("42");
        listaCodigosCVM.add("43");
        listaCodigosCVM.add("102");
        listaCodigosCVM.add("75");

        boolean verificaISINObrigatorio;
        String tipoAtivo;
        String codigoTipoCVM = null;

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();

            //Verificar se tipo do ativo obriga Informar ISIN
            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
                //Capturando a identificação do ativo
                SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
                String[] id;
                id = identifica.retornaIdentificador(listaIdentificadoresAtivo);
                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();
                verificaISINObrigatorio = false;
                try {
                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();
                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
                            codigoTipoCVM = listaIdAtivos.getId();
                            //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
                            if (listaCodigosCVM.contains(codigoTipoCVM)) {
                                verificaISINObrigatorio = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }

                if (verificaISINObrigatorio) {
                    //Obrigatório informar código ISIN
                    String codIsin;
                    codIsin = listaAtivos.getFinInstrmId().getISIN();
                    if (codIsin == null) {
                        errors.add("Obrigatório informar ISIN para ativo de Código CVM igual a "
                                + codigoTipoCVM + ". Verificar Ativo de código " + id[1]
                                + " igual a "
                                + id[0] + ". ");
                    }
                }
            }
        }
        return errors;
    }
}
