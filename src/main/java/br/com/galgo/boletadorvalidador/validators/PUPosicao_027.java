/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.BalanceForSubAccountUtils;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceInformation5;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import iso.std.iso._20022.tech.xsd.semt_003_001.TypeOfPrice11Code;
import java.util.List;
import jodd.bean.BeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Preço Unitário de valorização dos ativos, na data da posição do fundo ou carteira administrada
 * Deve ser informado para todos tipos de ativos
 *
 * @author joyce.oliveira
 */
@ValidatorType
class PUPosicao_027 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            PUPosicao_027.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste de PU da Posicao");

        /**
         * Obrigatorio informar código MRKT ou INDC, referente ao PU da Posição
         */
        List<OtherIdentification1> othrIdLst = null;
        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 subAcctDtls : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = subAcctDtls.getBalForSubAcct();

            int counter = 0;
            int PUPosicao = 0;
            for (AggregateBalanceInformation13 balForSubAcct : balForSubAcctLst) {
                counter++;
                PUPosicao = 0;

                BalanceForSubAccountUtils balUtils = new BalanceForSubAccountUtils(balForSubAcct);
                String ativoId = balUtils.getIdAsString();

                List<PriceInformation5> pricDtlsLst = balForSubAcct.getPricDtls();

                for (PriceInformation5 pricDtls : pricDtlsLst) {
                    //TypeOfPrice11Code tipoCodigo = pricDtls.getTp().getCd();
                    TypeOfPrice11Code tipoCodigo = (TypeOfPrice11Code) BeanUtil.
                            getPropertySilently(pricDtls, "tp.cd");

                    if (TypeOfPrice11Code.MRKT.equals(tipoCodigo) || TypeOfPrice11Code.INDC.
                            equals(tipoCodigo)) {
                        PUPosicao++;
                    }
                }

                if (PUPosicao == 0) {
                    errors.add(String.format(
                            "É obrigatório informar o código MRKT ou INDC,"
                            + " referente ao PU da Posição. Ver ativo nr. %s: %s "
                            + "<SubAcctDtls><BalForSubAcct><PricDtls>",
                            counter, ativoId
                    ));
                } else if (PUPosicao > 1) {
                    errors.add(String.format(
                            "Os Tipos de Ativos MRKT ou INDC,"
                            + " referentes ao PU da Posição,"
                            + " não devem ser informados mais de uma vez por ativo."
                            + " Verificar ativo nr. %s: %s"
                            + "<SubAcctDtls><BalForSubAcct><PricDtls>",
                            counter, ativoId
                    ));
                }

//                if (tipoCodigo.equals("MRKT") || tipoCodigo.equals("INDC")) {
//                    //log.debug("Quantidade de codigos MRKT ou INDC informados = {}", PUPosicao);
//                    if (PUPosicao > 1) {
//                        othrIdLst = balForSubAcct.getFinInstrmId().getOthrId();
//                        for (OtherIdentification1 listaId : othrIdLst) {
//                            tipoID = listaId.getId();
//                        }
//                        errors.add("Tipo de Ativo " + tipoCodigo + ", referente ao PU da Posição,"
//                                + " não deve ser informado mais de uma vez por ativo."
//                                + " Verificar ativo de Código CVM igual a " + tipoID + ". "
//                                + "<SubAcctDtls><BalForSubAcct><PricDtls>");
//                    }
//                }
//                PUPosicao = 0;
            }
        }
        return errors;
    }
}
