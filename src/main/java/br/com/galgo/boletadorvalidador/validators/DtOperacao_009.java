/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceInformation5;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Data da Operação deve ser:
 * Posterior ou igual à data da emissão do ativo; (Validado na Emissão)
 * Anterior ou igual à data do vencimento do ativo
 * Anterior ou igual à data do arquivo.
 *
 * @author joyce.oliveira
 */
@ValidatorType
class DtOperacao_009 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            DtOperacao_009.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste de Data da Operação");

        errors = Lists.newArrayList();

        List<SubAccountIdentification16> subAcctDtlsLista = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();
        XMLGregorianCalendar dataArquivo;
        XMLGregorianCalendar dataOperação;
        int comparacao;
        /**
         * Verificar Data do Arquivo
         */
        dataArquivo = doc.getSctiesBalAcctgRpt().getStmtGnlDtls().getStmtDtTm().getDt();

        for (SubAccountIdentification16 fundos : subAcctDtlsLista) {
            List<AggregateBalanceInformation13> balForSubAcct = fundos.getBalForSubAcct();
            for (AggregateBalanceInformation13 listaAtivos : balForSubAcct) {
                //Capturando a identificação do ativo
                Identificador identifica = new Identificador();
                SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
                String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                try {
                    List<PriceInformation5> prcDtlsLista = listaAtivos.getPricDtls();

                    /**
                     * Verificar Data do Vencimento
                     */
                    XMLGregorianCalendar dtVcmtAtivo = listaAtivos.getFinInstrmAttrbts().getMtrtyDt();

                    /**
                     * Verificar Data da Operação
                     */
                    for (PriceInformation5 detalhePreco : prcDtlsLista) {
                        try {
                            dataOperação = detalhePreco.getQtnDt().getDt();
                            comparacao = dataOperação.compare(dataArquivo);
                            if (comparacao == 1) {
                                errors.
                                        add(
                                        "Data de Operação do Ativo deve ser anterior ou igual a Data "
                                        + "do Arquivo. Foi informada Data de Operação: "
                                        + dataOperação+ " e Data do Arquivo: " + dataArquivo
                                        + ". Verificar ativo de código " + id[1] + " igual"
                                        + " a " + id[0]+ ". <BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                        + "<BalForSubAcct><PricDtls><QtnDt><Dt>");
                            }
                            comparacao = dataOperação.compare(dtVcmtAtivo);
                            if (comparacao == 1) {
                                errors.
                                        add(
                                        "Data de Operação do Ativo deve ser anterior ou igual a Data de "
                                        + "Vencimento. Foi informada Data de Operação: "
                                        + dataOperação+ " e Data do Vencimento: " + dtVcmtAtivo
                                        + ". Verificar ativo de código " + id[1] + " igual"
                                        + " a " + id[0]+". <BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                        + "<BalForSubAcct><PricDtls><QtnDt><Dt>");
                            }
                        } catch (NullPointerException e) {
                        }
                    }
                } catch (NullPointerException e) {
                }
            }
        }
        return errors;
    }
}
