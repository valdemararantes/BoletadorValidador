/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceInformation5;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Para Debentures : Principal = PU de Compra x Quantidade total
 * Para Termo RF, Título Publico e Título Privado: Principal = PU de Emissão x Quantidade Total
 *
 * @author joyce.oliveira
 */
@ValidatorType
class ValorPrincipal_024 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            ValorPrincipal_024.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Valor Principal");

        errors = Lists.newArrayList();

        String tipoAtivo = null;
        String codigoTipoCVM = null;
        boolean verificaValorPrincipal = false;
        BigDecimal valorPrincipalArquivo = null;
        String tipoCodigo = null;
        BigDecimal PUArquivo = null;
        BigDecimal qntdTotal = null;
        List<String> listaCodigosCVM = new ArrayList<>();

        //Códigos CVM dos tipos de ativos que devem informar Valor Principal
        listaCodigosCVM.add("193");
        listaCodigosCVM.add("42");
        listaCodigosCVM.add("43");
        listaCodigosCVM.add("196");
        listaCodigosCVM.add("75");

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();
            verificaValorPrincipal = false;

            //Verificar se tipo do ativo obriga Valor Principal
            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();

                try {
                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();
                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
                            codigoTipoCVM = listaIdAtivos.getId();
                            //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
                            //Se for código 147,193,42,43, 196 ou 75 deve verificar se informou OFFR
                            if (listaCodigosCVM.contains(codigoTipoCVM)) {
                                verificaValorPrincipal = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }

                if (verificaValorPrincipal) {
                    //Capturando o Valor Principal
                    try {
                        valorPrincipalArquivo = listaAtivos.getAcctBaseCcyAmts().getBookVal().
                                getAmt().getValue();
                        //log.debug("Valor Principal = {}", valorPrincipalArquivo);
                    } catch (NullPointerException e) {
                        //log.debug("Não informou Valor Principal.");
                    }
                    //Capturando Quantidade Total
                    try {
                        qntdTotal = listaAtivos.getAggtBal().getQty().getQty().getQty().getUnit();
                        //log.debug("Quantidade Total = {}", qntdTotal);
                    } catch (NullPointerException e) {
                        //log.debug("Não informou quantidade total.");
                    }

                    //Se for Termo de RF verificar PU de Emissão
                    if (codigoTipoCVM.equals("42") || codigoTipoCVM.equals("43")) {
                        try {
                            PUArquivo = listaAtivos.getFinInstrmAttrbts().getSbcptPric().getVal().
                                    getAmt().getValue();
                        } catch (NullPointerException e) {
                        }

                     //Se Debentures ou Títulos, deve verificar PU da Compra
                    } else {
                        List<PriceInformation5> pricDtlsLst = listaAtivos.getPricDtls();

                        for (PriceInformation5 listaDetalhesPrecos : pricDtlsLst) {
                            tipoCodigo = listaDetalhesPrecos.getTp().getCd().toString();
                            if (tipoCodigo.equals("OFFR")) {
                                PUArquivo = listaDetalhesPrecos.getVal().getAmt().getValue();
                                break;
                            }
                        }
                    }

                   // log.debug("PU encontrado = {}, ativo de código {}", PUArquivo, codigoTipoCVM);

                    //Conferindo se Valor Principal =  PU * Qntd Total
                    try {
                        if (valorPrincipalArquivo.compareTo(qntdTotal.multiply(PUArquivo)) != 0) {
                            if (codigoTipoCVM.equals("42") || codigoTipoCVM.equals("43")) {
                                errors.
                                        add("Valor Principal deve ser igual a PU de Emissão multiplicado "
                                        + " pela Quantidade Total. Foi informado Valor Principal "
                                        + "igual a " + valorPrincipalArquivo + ", valor de PU de "
                                        + "Emissão igual a " + PUArquivo
                                        + ", e valor da Quantidade "
                                        + "Total igual a " + qntdTotal
                                        + ". Verificar ativo do tipo "
                                        + "CVM igual a " + codigoTipoCVM + ".");
                            } else {
                                errors.
                                        add("Valor Principal deve ser igual a PU de Compra multiplicado "
                                        + " pela Quantidade Total. Foi informado Valor Principal "
                                        + "igual a " + valorPrincipalArquivo + ", valor de PU de "
                                        + "Emissão igual a " + PUArquivo
                                        + ", e valor da Quantidade "
                                        + "Total igual a " + qntdTotal
                                        + ". Verificar ativo de código "
                                        + "CVM igual a " + codigoTipoCVM + ".");
                            }
                        }
                    } catch (NullPointerException e) {
                        if (codigoTipoCVM.equals("42") || codigoTipoCVM.equals("43")) {
                                errors.
                                        add("Valor Principal deve ser igual a PU de Emissão multiplicado "
                                        + " pela Quantidade Total. Foi informado Valor Principal "
                                        + "igual a " + valorPrincipalArquivo + ", valor de PU de "
                                        + "Emissão igual a " + PUArquivo
                                        + ", e valor da Quantidade "
                                        + "Total igual a " + qntdTotal
                                        + ". Verificar ativo do tipo "
                                        + "CVM igual a " + codigoTipoCVM + ".");
                            } else {
                                errors.
                                        add("Valor Principal deve ser igual a PU de Compra multiplicado "
                                        + " pela Quantidade Total. Foi informado Valor Principal "
                                        + "igual a " + valorPrincipalArquivo + ", valor de PU de "
                                        + "Emissão igual a " + PUArquivo
                                        + ", e valor da Quantidade "
                                        + "Total igual a " + qntdTotal
                                        + ". Verificar ativo de código "
                                        + "CVM igual a " + codigoTipoCVM + ".");
                            }

                    }
                }
            }
        }
        return errors;
    }
}
