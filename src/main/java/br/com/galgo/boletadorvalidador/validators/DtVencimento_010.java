/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Data do Vencimento deve ser: Posterior ou igual à data do arquivo: Posterior ou igual à data da
 * operação / compra do ativo;(Validado na Data Operação) Posterior ou igual à data da emissão do
 * ativo.(Validado na Data Emissão)
 *
 * @author joyce.oliveira
 */
@ValidatorType
class DtVencimento_010 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            DtVencimento_010.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste de Data do Vencimento");

        errors = Lists.newArrayList();

        boolean verificaDataVencimento = false;
        String tipoAtivo = null;
        String codigoTipoCVM = null;

        List<String> listaCodigosCVM = new ArrayList<>();
        //Códigos CVM dos tipos de ativos que devem seguir regras da Data de Vencimento
        listaCodigosCVM.add("147");
        listaCodigosCVM.add("193");
        listaCodigosCVM.add("48");
        listaCodigosCVM.add("102");
        listaCodigosCVM.add("39");
        listaCodigosCVM.add("40");
        listaCodigosCVM.add("183");
        listaCodigosCVM.add("43");
        listaCodigosCVM.add("42");
        listaCodigosCVM.add("196");
        listaCodigosCVM.add("75");

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();

            //Verificar se tipo do ativo obriga a validar Data de Vencimento
            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();
                verificaDataVencimento = false;

                 //Capturando a identificação do ativo
                Identificador identifica = new Identificador();
                SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
                String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                try {

                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();
                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {

                            codigoTipoCVM = listaIdAtivos.getId();
                            //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
                            //Se for código 147,193,42,43, 196 ou 75 deve verificar Data Emissão
                            if (listaCodigosCVM.contains(codigoTipoCVM)) {
                                verificaDataVencimento = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }

                if (verificaDataVencimento) {


                    List<SubAccountIdentification16> subAcctDtlsLista = doc.getSctiesBalAcctgRpt().
                            getSubAcctDtls();
                    XMLGregorianCalendar dataArquivo;
                    int comparacao;

                    /**
                     * Verificar Data do Arquivo
                     */
                    dataArquivo = doc.getSctiesBalAcctgRpt().getStmtGnlDtls().getStmtDtTm().getDt();

                    /**
                     * Verificar Data do Vencimento
                     */
                    XMLGregorianCalendar dtVcmtAtivo = listaAtivos.getFinInstrmAttrbts().
                            getMtrtyDt();
                    //log.debug("Data Vencimento Ativo = " + dtVcmtAtivo);
                    try {
                        comparacao = dtVcmtAtivo.compare(dataArquivo);
                        if (comparacao == -1) {
                            errors.
                                    add(
                                    "Data de Vencimento do Ativo deve ser posterior ou igual a Data "
                                    + "do Arquivo. Foi informada Data de Vencimento: "
                                    + dtVcmtAtivo
                                    + " e Data do Arquivo: " + dataArquivo
                                    + " verificar Ativo de Código CVM igual a " + codigoTipoCVM
                                    + " Verificar ativo de código "+id[1]+" igual a "+ id[0]
                                    + ". <SubAcctDtls><BalForSubAcct><FinInstrmAttrbts><MtrtyDt>");
                        }
                    } catch (NullPointerException e) {
                    }
                }
            }
        }
        return errors;
    }
}
