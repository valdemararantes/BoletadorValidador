/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.GenericIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Informar o campo(Número anexado ao Cupom - Coupom Atached Number)
 *
 * Para SchmeNm -> Tabela de Indexadores
 * Para Issr    -> BVMF
 * Para Id, dependo do tipo de ativo
 * TODO - verificar com Ana se são apenas os tipos informados nos exemplos
 *
 * @author joyce.oliveira
 */
@ValidatorType
class Indexador_034 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            Indexador_034.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Indexador");

        errors = Lists.newArrayList();

        boolean verificaIndexadores = false;
        String tipoAtivo = null;
        String codigoTipoCVM = null;

        List<String> listaCodigosCVM = new ArrayList<>();
        //Códigos CVM dos tipos de ativos que devem informar Valor Principal
        listaCodigosCVM.add("147");
        listaCodigosCVM.add("193");
        listaCodigosCVM.add("42");
        listaCodigosCVM.add("43");
        listaCodigosCVM.add("196");
        listaCodigosCVM.add("75");

         List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

//        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
//            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
//                    getBalForSubAcct();
//            verificaIndexadores = false;
//
//            //Verificar se tipo do ativo obriga Indexador
//            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
//                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
//                        getOthrId();
//
//                for (OtherIdentification1 listaIdAtivos : othrIdLst) {
//                    tipoAtivo = listaIdAtivos.getTp().getPrtry();
//                    //log.debug("Tipo Identificacao: {}", tipoAtivo);
//                    if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
//                        codigoTipoCVM = listaIdAtivos.getId();
//                        //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
//                        //Se for código 147,193,42,43, 196 ou 75 deve verificar se informou OFFR
//                        if (listaCodigosCVM.contains(codigoTipoCVM)) {
//                            verificaIndexadores = true;
//                            break;
//                        }
//                    }
//                }
//
//                if(verificaIndexadores){
//                    //Verificar se na tag <CpnAttchdNb> foi informado LOGN
//                    String codigoIndexador = listaAtivos.getFinInstrmAttrbts().getCpnAttchdNb().getLng().toString();
//                    //Verificar se tag <SchmeNm> preenchida com Tabela de Indexadores
//                    //Verificar se tag <Issr> preenchida com BVMF
//                    //Verificar se tag <Id> contém valor referente ao indexador
//
//                }
//            }
//        }
        return errors;
    }
}