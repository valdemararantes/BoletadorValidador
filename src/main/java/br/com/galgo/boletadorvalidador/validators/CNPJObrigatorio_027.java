/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *
 * @author joyce.oliveira
 */
@ValidatorType
class CNPJObrigatorio_027 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            CNPJObrigatorio_027.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste CNPJ Obrigatorio");

        errors = Lists.newArrayList();
        Identificador identifica = new Identificador();

        List<String> listaCodigosCVM = new ArrayList<>();
        //Códigos CVM dos tipos de ativos que devem informar CNPJ
        listaCodigosCVM.add("37");

        boolean verificaCNPJObrigatorio = false;
        String tipoAtivo = null;
        String codigoTipoCVM = null;
        String cnpj = null;

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();

            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
                //Capturando a identificação do ativo
                SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
                String[] id;
                id = identifica.retornaIdentificador(listaIdentificadoresAtivo);
                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();
                verificaCNPJObrigatorio = false;

                try {
                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();
                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
                            codigoTipoCVM = listaIdAtivos.getId();
                            //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
                            if (listaCodigosCVM.contains(codigoTipoCVM)) {
                                verificaCNPJObrigatorio = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }

                if (verificaCNPJObrigatorio) {

                    for (OtherIdentification1 listaIds : othrIdLst) {
                        if ("CNPJ".equals(listaIds.getTp().getCd())) {
                            cnpj = listaIds.getTp().getCd();
                            break;
                        }
                    }
                    //Se string cnpj é nula, não foi informado CNPJ, retorna erro
                    if (cnpj == null) {
                        errors.add("Obrigatório informar CNPJ para ativo de Código CVM igual a "
                                + codigoTipoCVM + ". Verificar Ativo de código " + id[1]
                                + " igual a "
                                + id[0] + ". ");
                    }
                }
            }
        }
        return errors;
    }
}
