/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.ForeignExchangeTerms14;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Detalhes de Câmbio (Foreign Exchange Details)
 *
 * Obrigatório para os seguintes tipos de ativo:
 *
 * Foward de Moeda;
 * Opções de Moeda
 *
 * @author joyce.oliveira
 */

@ValidatorType
class DetalheCambio_026 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            DetalheCambio_026.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste de Detalhes Cambio");

        errors = Lists.newArrayList();

        //Verificar se ativo do Tipo Opções de Moeda ou Foward de Moeda

        //Códigos CVM dos tipos de ativos que devem informar Estilo de Opção
        List<String> listaCodigosCVM = new ArrayList<>();
        listaCodigosCVM.add("48");
        listaCodigosCVM.add("39");
        listaCodigosCVM.add("40");

        boolean verificaDetalhesCambio;
        String tipoAtivo;
        String codigoTipoCVM = null;
        List<ForeignExchangeTerms14> fxDtlsLst;

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {


            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();
            //Verificar se tipo do ativo obriga Informar Detalhes de Cambio
            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {

                //Capturando a identificação do ativo
                Identificador identifica = new Identificador();
                SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
                String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();

                verificaDetalhesCambio = false;
                try {
                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {

                        tipoAtivo = listaIdAtivos.getTp().getPrtry();
                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
                            codigoTipoCVM = listaIdAtivos.getId();
                            //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
                            if (listaCodigosCVM.contains(codigoTipoCVM)) {
                                verificaDetalhesCambio = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }

                //Se variavel verificaDetalhesCambio igual a true, deve conferir se seção foi informada
                if (verificaDetalhesCambio) {
                    try {

                        fxDtlsLst = listaAtivos.getFXDtls();

                        if (fxDtlsLst.isEmpty()) {
                            errors.add("Para ativo de tipo CVM igual a " + codigoTipoCVM
                                    + " é obrigatório informar Detalhes de Cambio na Seção"
                                    + "ForeignExchangeDetails. "
                                    + "Verificar ativo de código "+id[1]+" igual a "+ id[0]+"."
                                    + "<SubAcctDtls><BalForSubAcct><FXDtls>");
                        }

                    } catch (NullPointerException e) {
                        errors.add("Para ativo de tipo CVM igual a " + codigoTipoCVM
                                + " é obrigatório informar Detalhes de Cambio na Seção "
                                + "ForeignExchangeDetails. "
                                + " Verificar ativo de código "+id[1]+" igual a "+ id[0]+". "
                                + "ForeignExchangeDetails. <SubAcctDtls><BalForSubAcct><FXDtls>");
                    }
                }
            }
        }
        return errors;
    }
}
