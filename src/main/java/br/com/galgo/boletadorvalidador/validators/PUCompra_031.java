/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceInformation5;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Código OFFR, obrigatório para:
 * -Debentures;
 * -Títulos Publicos;
 * -Títulos Privados
 * - Opções Flexíveis
 * TODO - como verificar se é opção flexível e não outro tipo, não existe exemplo de opção flexivel.
 *
 * @author joyce.oliveira
 */
@ValidatorType
class PUCompra_031 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            PUCompra_031.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste PU de Compra");

        errors = Lists.newArrayList();

        String tipoAtivo = null;
        String codigoTipoCVM = null;
        boolean verificaPUCompra = false;
        String tipoPreco = null;

        List<String> listaCodigosCVM = new ArrayList<>();

        //Códigos CVM dos tipos de ativos que devem informar PU da Compra
        listaCodigosCVM.add("193");
        listaCodigosCVM.add("39");
        listaCodigosCVM.add("40");
        listaCodigosCVM.add("196");
        listaCodigosCVM.add("75");

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();
            verificaPUCompra = false;

            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
                /**
                 * Conferir se é Debenture, Título Publico ou título Privado
                 */
                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();

                //Capturando a identificação do ativo
                Identificador identifica = new Identificador();
                SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
                String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                try {
                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();
                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
                            codigoTipoCVM = listaIdAtivos.getId();
                            //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
                            //Se for código 193,39,40,196 ou 75 deve verificar se informou OFFR
                            if (listaCodigosCVM.contains(codigoTipoCVM)) {
                                verificaPUCompra = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }
                //log.debug("Varialvel Verifica PU de Compra = {}", verificaPUCompra);
                if (verificaPUCompra) {
                   // log.debug("Tem que verificar PU de compra pois é do "
                    //        + "tipo {}", codigoTipoCVM);
                    List<PriceInformation5> pricDtlsLst = listaAtivos.getPricDtls();
                    int PUCompra = 0;

                    try {
                        for (PriceInformation5 listaPrecos : pricDtlsLst) {
                            tipoPreco = listaPrecos.getTp().getCd().toString();
                            if (tipoPreco.equals("OFFR")) {
                                PUCompra++;
                            }
                        }
                    } catch (NullPointerException e) {
                        //log.debug("<Cd> Nulo!!!");
                    }

                    //log.debug("Quantidade de códigos OFFR informado para ativo: {}", PUCompra);

                    if (PUCompra > 1) {
                        errors.add("Código OFFR, referente a PU da Compra,"
                                + " foi informado mais de uma vez para "
                                + "ativo do tipo CVM igual a " + codigoTipoCVM +
                                ". Código pode ser informado apenas uma vez para cada ativo. "
                                + "Verificar ativo de código "+id[1]+" igual a "+ id[0]+". "
                                + "<BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                + "<BalForSubAcct><PricDtls><Tp><Cd>");
                    }
                    if (PUCompra == 0) {
                        errors.add("Para ativo de tipo CVM igual a " +codigoTipoCVM
                                + ", obrigatório informar código OFFR, referente a PU da Compra. "
                                + "Verificar ativo de código "+id[1]+" igual a "+ id[0]+". "
                                + "<BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                + "<BalForSubAcct><PricDtls><Tp><Cd>");
                    }
                }
            }
        }
        return errors;
    }
}