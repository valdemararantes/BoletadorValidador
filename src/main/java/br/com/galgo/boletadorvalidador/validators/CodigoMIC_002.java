package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.BalanceForAccountUtils;
import br.com.galgo.boletadorvalidador.utils.BalanceForSubAccountUtils;
import br.com.galgo.boletadorvalidador.utils.Config;
import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 *
 * @author joyce.oliveira
 */
/**
 * Código MIC
 *
 * Código ISO 10383 Market Identification Code composto de 4 caracteres (Alfa-numérico)
 *
 * @author joyce.oliveira
 */
@ValidatorType
class CodigoMIC_002 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(CodigoMIC_002.class);
    private static final List<String> codigosMIC = Lists.newArrayList(Config.getStringList(
            "codigos_mic"));
    private List<String> errors = Lists.newArrayList();

    @Override
    public List<String> validate(Document doc) {
        log.debug("Início do teste Código MIC");

        /**
         * Verificar Códigos MIC dos Fundos, se existirem
         */
        validateFundos(doc.getSctiesBalAcctgRpt().getBalForAcct());

        List<SubAccountIdentification16> subAcctDtls = doc.getSctiesBalAcctgRpt().getSubAcctDtls();
        for (SubAccountIdentification16 subAcct : subAcctDtls) {
            validateAtivos(subAcct.getBalForSubAcct());
        }

        return errors;
    }

    boolean verificaIso10383(String codigoMIC) {
        return codigosMIC.contains(codigoMIC);
    }

    /**
     * Verifica se os ativos que devem possuir código MIC de fato possuem.
     * O código MIC é obrigatório para os seguintes tipos de ativos:
     * <li>Ações
     * <li>Operações compromissadas
     * <li>Cotas de Fundos negociadas em bolsa
     * <li>Debentures
     * <li>Empréstimos de Ações
     * <li>Contratos Futuros
     * <li>Opções
     * <li>Swaps
     * <li>Termos
     * <li>Títulos
     *
     * @param balForSubAcctList
     */
    private void validateAtivos(List<AggregateBalanceInformation13> balForSubAcctList) {
        final List<String> codAtivoListNeedMIC = Lists.newArrayList(
                "EQUI", // Ações
                "REPO", // Operações Compromissadas
                /*
                Por não ser possível ser diferenciar cotas de fundos negociadas em bolsa das não
                negociadas em bolsa, o Código MIC não é obrigatório. (VAN - 31/07/2015)
                "SHAR", // Cotas de Fundos Negociadas em Bolsa
                 */
                "DEBE", // Debêntures
                "LOAN", // Empréstimos de Ações
                "FUTU", // Contratos Futuros
                "OPTN", // Opções
                "SWAP", // SWAPS
                "FWRD", // Termo
                "GOVE", "CORP" // Títulos (Públicos e Privados)
        );

        int counter = 0;
        for (AggregateBalanceInformation13 balForSubAcct : balForSubAcctList) {
            counter++;
            try {
                //Verificar identificação do ativo
                SecurityIdentification14 finInstrmId = balForSubAcct.getFinInstrmId();
                String[] id = new Identificador().retornaIdentificador(finInstrmId);

                BalanceForSubAccountUtils balForSubAcctUtils = new BalanceForSubAccountUtils(
                        balForSubAcct);
                String codMIC = balForSubAcctUtils.getCodMIC();
                String tpANBIMA = balForSubAcctUtils.getCodIdentAtivo();

                // Validando a obrigatoriedade do código MIC para tipos de ativos específicos
                if (StringUtils.isNotBlank(tpANBIMA) && codAtivoListNeedMIC.contains(tpANBIMA)) {
                    if (StringUtils.isBlank(codMIC)) {
                        errors.add(String.format(
                                "O Código MIC é obrigatório para ativos com tipo ANBIMA \"%s\". "
                                + "Verificar ativo nr. %s: (%s).",
                                tpANBIMA, counter, balForSubAcctUtils.getIdAsString()
                        ));

                    }
                }

                // Valida se o código pertence à tabela ISO 10383
                if (StringUtils.isNotBlank(codMIC) && !verificaIso10383(codMIC)) {
                    errors.add(String.format(
                            "O Código MIC \"%s\" não existe na ISO 10383."
                            + " Verificar ativo nr. %s: %s",
                            codMIC, counter, balForSubAcctUtils.getIdAsString()
                    ));
                }
            } catch (NullPointerException e) {
                log.error(
                        String.format(
                                "Ativo nr. %s não possui código MIC: %s",
                                counter, balForSubAcct.getFinInstrmId().getISIN()),
                        e);
            }
        }
    }

    /**
     * Se o fundo possuir código MIC, verifica se pertence à tabela de códigos MIC.
     *
     * @param balForAcctList
     */
    private void validateFundos(List<AggregateBalanceInformation13> balForAcctList) {
        for (AggregateBalanceInformation13 balForAcct : balForAcctList) {
            try {
                BalanceForAccountUtils balForAcctUtils = new BalanceForAccountUtils(balForAcct);
                String micFundo = balForAcctUtils.getCodMIC();
                if (StringUtils.isBlank(micFundo)) {
                    log.debug("Fundo[{} = {}] sem código MIC", balForAcctUtils.getIdAsString());
                    continue;
                }

                //Capturando a identificação do ativo
                String[] id = new Identificador().retornaIdentificador(balForAcct.getFinInstrmId());

                if (!verificaIso10383(micFundo)) {
                    errors.add(
                            "Código " + micFundo
                            + " não existe na ISO 10383."
                            + " Verificar ativo de código " + id[1] + " igual a " + id[0]
                            + " <SctiesBalAcctgRpt><BalForAcct>"
                            + "<FinInstrmAttrbts><PlcOfListg><Id><MktIdrCd>");
                }

            } catch (NullPointerException e) {
            }
        }

    }
}
