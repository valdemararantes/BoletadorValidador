/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.BalanceBreakdownUtils;
import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceInformation5;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceType12Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.TypeOfPrice11Code;
import java.math.BigDecimal;
import java.util.List;
import jodd.bean.BeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
//@ValidatorType
class CotasEmitir_013 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            CotasEmitir_013.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste de Cotas a Emitir");

        errors = Lists.newArrayList();

        /**
         * Verificar Cotas a Emitir
         *
         * Valor financeiro = Quantiade de cotas * Valor da Cota
         */
        BigDecimal qtdCotas = null;
        BigDecimal valorFinanceiroCotasEmitir = null;
        BigDecimal valorCota = null;
        List<AggregateBalanceInformation13> balForAcctList = null;
        balForAcctList = doc.getSctiesBalAcctgRpt().getBalForAcct();

        for (AggregateBalanceInformation13 balForAcct : balForAcctList) {
            List<SubBalanceInformation6> balBrkdwnLst = balForAcct.getBalBrkdwn();

            for (SubBalanceInformation6 balBrkdwn : balBrkdwnLst) {

                BalanceBreakdownUtils balBrkdwnUtils = new BalanceBreakdownUtils(balBrkdwn, balForAcct);

                SecuritiesBalanceType12Code codigoTipo = balBrkdwnUtils.getCodigoTipo();
                if (SecuritiesBalanceType12Code.PEND.equals(codigoTipo)) {
                    qtdCotas = balBrkdwnUtils.getQtdCotas();

                    //Verificar qual o valor financeiro
                    valorFinanceiroCotasEmitir = balBrkdwnUtils.getValorFinanceiro();
                }
            }

            //Capturar o valor da Cota
            List<PriceInformation5> pricDtlsLst = balForAcct.getPricDtls();

            //Capturando a identificação do ativo
            Identificador identifica = new Identificador();
            SecurityIdentification14 listaIdentificadoresAtivo = balForAcct.getFinInstrmId();
            String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

            for (PriceInformation5 pricDtls : pricDtlsLst) {
                TypeOfPrice11Code typeCode = (TypeOfPrice11Code) BeanUtil.getPropertySilently(pricDtls, "tp.cd");
                if (TypeOfPrice11Code.MRKT.equals(typeCode)) {
                    valorCota = (BigDecimal) BeanUtil.getPropertySilently(pricDtls, "val.amt.value");
                }
            }

            // Se Valor financeiro for declarado,
            // conferir se Valor financeiro = Quantiade de cotas * Valor da Cota
            try {
                if (valorFinanceiroCotasEmitir != null
                        && valorFinanceiroCotasEmitir.compareTo(valorCota.multiply(qtdCotas)) != 0) {
                    errors.add(buildErrorMsg(valorFinanceiroCotasEmitir, valorCota, qtdCotas, id));
                }
            } catch (NullPointerException e) {
                errors.add(buildErrorMsg(valorFinanceiroCotasEmitir, valorCota, qtdCotas, id));
            }
        }
        return errors;
    }

    private String buildErrorMsg(BigDecimal valorFinanceiroCotasEmitir, BigDecimal valorCota,
            BigDecimal qntdCotas, String[] id) {

        return "Valor total de Cotas a Emitir, representado pelo código PEND, deve ser igual ao "
                + "Valor da Cota multiplicado pela Quantidade de Cotas."
                + "Foi informado Valor de Cotas a Emitir igual a "
                + getObjStr(valorFinanceiroCotasEmitir) + ", Valor da Cota "
                + "igual a " + getObjStr(valorCota) + ", e Quantidade de Cotas "
                + "igual a " + getObjStr(qntdCotas)
                + (
                    id != null ?
                        (". Verificar ativo de código " + getObjStr(id[1]) + " igual a " + getObjStr(id[0])) : "")
                + ". <BsnsMsg><SctiesBalAcctgRpt><BalForAcct><BalBrkdwn><SubBalTp><Cd>";
    }

    private String getObjStr(Object obj) {
        return obj == null ? "<Valor Não Encontrado>" : obj.toString();
    }
}
