/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * O campo Ativo Base da Opção (Underlying Financial Instrument Identification)
 * Verificar para seguintes tipos de ativos:
 * Compromisssadas; Opções de Ações; Opções de Derivativos; Opções Flexíveis
 *
 * @author joyce.oliveira
 */
@ValidatorType
class AtivoBase_036 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            AtivoBase_036.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Ativo Base");

        errors = Lists.newArrayList();

        boolean verificaAtivoBase = false;
        String tipoAtivo = null;
        String codigoTipoCVM = null;

        List<String> listaCodigosCVM = new ArrayList<>();
        //Códigos CVM dos tipos de ativos que devem informar Ativo Base
        listaCodigosCVM.add("147");
        listaCodigosCVM.add("39");
        listaCodigosCVM.add("40");
        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();
            verificaAtivoBase = false;

            //Verificar se tipo do ativo obriga Percentua do Indexador
            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();
                //Capturando a identificação do ativo
                Identificador identifica = new Identificador();
                SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
                String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                try {

                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();
                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
                            codigoTipoCVM = listaIdAtivos.getId();
                            //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);

                            if (listaCodigosCVM.contains(codigoTipoCVM)) {
                                verificaAtivoBase = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }
                if(verificaAtivoBase){
                    //Verificar se informou <IndxRateBsis>
                    try {
                        List<SecurityIdentification14> undrlygFinInstrmIdLst = listaAtivos.
                                getFinInstrmAttrbts().getUndrlygFinInstrmId();

                        if(undrlygFinInstrmIdLst.isEmpty()){
                             errors.add("Obrigatório informar Ativo Base da Opção para tipo de ativo"
                                + " de Código CVM igual a "+codigoTipoCVM+". "
                                     + "Verificar ativo de código "+id[1]+" igual a "+ id[0]+". "
                                     + "<BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                     + "<BalForSubAcct><FinInstrmAttrbts><UndrlygFinInstrmId>");
                        }

                    } catch (NullPointerException e) {
                        errors.add("Obrigatório informar Ativo Base da Opção para tipo de ativo"
                                + " de Código CVM igual a "+codigoTipoCVM+". "
                                + "Verificar ativo de código "+id[1]+" igual a "
                                + id[0]+". <BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                + "<BalForSubAcct><FinInstrmAttrbts><UndrlygFinInstrmId>");
                    }
                }
            }
        }
        return errors;
    }
}

