/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * O campo Identificação <Id> deverá ser preenchido com o tipo de conta, ex: “D” de Depósito
 * O campo Emissor <Issr> deverá ser preenchido com ANBIMA.
 * O campo Nome da Identificação  <SchmeNm> deverá ser preenchido com Tipo de Conta.
 *
 * @author joyce.oliveira
 */
@ValidatorType
class Caixa_038 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            Caixa_038.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Caixa");

        errors = Lists.newArrayList();

        String tipoAtivo = null;
        String codigoTipoCVM = null;
        boolean verificaCaixa = false;
        int contErros = 0;

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();

            //Verificar se tipo do ativo igual a Caixa, código 94
            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();

                //Capturando a identificação do ativo
                Identificador identifica = new Identificador();
                SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.
                        getFinInstrmId();
                String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                verificaCaixa = false;
                try {
                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();

                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        try {
                            if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
                                //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
                                codigoTipoCVM = listaIdAtivos.getId();
                                if (codigoTipoCVM.equals("94")) {
                                    verificaCaixa = true;
                                    break;
                                }
                            }
                        } catch (NullPointerException e) {
                            //log.debug("<Prtry> nulo!");
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }

                //Se ativo for do tipo Caixa, deve verificar Identificação proprietária
                //e Código CFI
                if (verificaCaixa) {
                    String emissor;
                    String tipoConta;
                    String identificacao = null;

                    //Para Caixa a identificação deve ser:
                    //Código ISIN reduzido (6 caracteres)
                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();
                        String idCaixa;
                        //Se tiver Identificação Proprietária, representada por 'Instituição Financeira'
                        if ("Instituição Financeira".equals(tipoAtivo)) {
                            idCaixa = listaIdAtivos.getId();
                            //Verificar se a Identificação possui até 6 caracteres
                            if (idCaixa.length() > 6) {
                                errors.add("Identificação Proprietária para ativo de Código CVM "
                                        + "igual a "+codigoTipoCVM+" não pode ter mais que "
                                        + "6 caracteres, foi informado "+idCaixa+"."
                                        + "<BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                        + "<BalForSubAcct><FinInstrmId><OthrId><Id>");
                            }
                        }
                    }

                    //<AltrnClssfctn> obrigatório para Caixa
                    //<Id> igual a "D", "P", "N", "I", ou "J"
                    List<String> listaIDCaixa = new ArrayList<>();
                    //Códigos CVM dos tipos de ativos que devem informar Cupom
                    listaIDCaixa.add("D");
                    listaIDCaixa.add("P");
                    listaIDCaixa.add("N");
                    listaIDCaixa.add("I");
                    listaIDCaixa.add("J");

                    try {
                        identificacao = listaAtivos.getFinInstrmAttrbts().getClssfctnTp().
                                getAltrnClssfctn().getId();
                        if (!listaIDCaixa.contains(identificacao)) {
                            errors.
                                    add(
                                    "Obrigatório informar na seção AlternateClassification, a "
                                    + "Identificação do tipo de conta:"
                                    + "“D” – Depósito; “P” – Poupança; “N” – Numerário; "
                                    + "“I” – Investimento ou “J” – Judicial, para tipo de "
                                    + "ativo de Código CVM igual a 94. Foi informado "
                                    + identificacao + ". "
                                    + "Verificar ativo de código " + id[1] + " igual a " + id[0]
                                    + ". <BsnsMsg><SctiesBalAcctgRpt>"
                                    + "<SubAcctDtls><BalForSubAcct><FinInstrmAttrbts>"
                                    + "<ClssfctnTp><AltrnClssfctn><Id>");
                        }
                    } catch (NullPointerException e) {
                        errors.add("Obrigatório informar na seção AlternateClassification, a "
                                + "Identificação conforme tipo da conta: "
                                + "“D” – Depósito; “P” – Poupança; “N” – Numerário; "
                                + "“I” – Investimento ou “J” – Judicial, para tipo de "
                                + "ativo de Código CVM igual a 94. Foi informado "
                                + identificacao + "."
                                + "Verificar ativo de código " + id[1] + " igual a " + id[0]
                                + "<BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                + "<BalForSubAcct><FinInstrmAttrbts>"
                                + "<ClssfctnTp><AltrnClssfctn><Id>");
                    }

                    //Verificando se <Issr>, Emissor, igual a ANBIMA
                    try {
                        emissor = listaAtivos.getFinInstrmAttrbts().getClssfctnTp().
                                getAltrnClssfctn().getIssr();
                        if (!emissor.equals("ANBIMA")) {
                            errors.
                                    add("Obrigatório informar na seção AlternateClassification, o "
                                    + "Emissor igual a 'ANBIMA', para tipo de ativo de Código "
                                    + "CVM igual a 94, foi informado "
                                    + emissor + ". Verificar ativo de código " + id[1] + " igual a "
                                    + id[0] + ". "
                                    + "<BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                    + "<BalForSubAcct><FinInstrmAttrbts>"
                                    + "<ClssfctnTp><AltrnClssfctn><Issr>");
                        }
                    } catch (NullPointerException e) {
                        errors.
                                add("Obrigatório informar na seção AlternateClassification, o "
                                + "Emissor igual a 'ANBIMA', para tipo de ativo de Código "
                                + "CVM igual a 94. "
                                + "Verificar ativo de código " + id[1] + " igual a " + id[0] + ". "
                                + "<BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                + "<BalForSubAcct><FinInstrmAttrbts>"
                                + "<ClssfctnTp><AltrnClssfctn><Issr>");
                    }

                    //Verificando se <SchmeNm> igual a Tipo da conta
                    try {
                        tipoConta = listaAtivos.getFinInstrmAttrbts().getClssfctnTp().
                                getAltrnClssfctn().getSchmeNm();
                        if (!tipoConta.equals("Tipo de Conta")) {
                            errors.
                                    add("Para tipo de ativo de Código CVM igual a 94, "
                                    + "deve ser informado na seção AlternateClassification, o "
                                    + "Nome da Identificação igual a 'Tipo de Conta',"
                                    + " foi informado " + tipoConta + ". "
                                    + "Verificar ativo de código " + id[1] + " igual a " + id[0]
                                    + "<BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                    + "<BalForSubAcct><FinInstrmAttrbts>"
                                    + "<ClssfctnTp><AltrnClssfctn><SchmeNm>");
                        }
                    } catch (NullPointerException e) {
                        errors.
                                add("Para tipo de ativo de Código CVM igual a 94, "
                                + "deve ser informado o Nome da Identificação na seção "
                                + "AlternateClassification, com o valor igual a 'Tipo de Conta'. "
                                + "Verificar ativo de código " + id[1] + " igual a " + id[0] + ". "
                                + "<BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls>"
                                + "<BalForSubAcct><FinInstrmAttrbts>"
                                + "<ClssfctnTp><AltrnClssfctn><SchmeNm>");
                    }
                }
            }
        }
        return errors;
    }
}
