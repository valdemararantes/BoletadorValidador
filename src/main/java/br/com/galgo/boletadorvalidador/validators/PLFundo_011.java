/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AdditionalBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceInformation5;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceAccountingReportV04;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.TypeOfPrice11Code;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * @author joyce.oliveira
 */
@ValidatorType
class PLFundo_011 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(PLFundo_011.class);
    private final int MARGEM_ERR = 500;
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste de PL do Fundo");

        /**
         * PL do Fundo – PL = Valor dos Ativos + Valores a Receber – Valores a Pagar – Cotas a
         * Emitir – Cotas a Resgatar.
         *
         * PL = Quantidade de Cotas X Valor das Cotas +/- 500
         *
         * Valor de corretagem e provisões não deve ser informado no Patrimônio Líquido.
         */
        BigDecimal valorCotas = null;
        BigDecimal quantidadeCotas = null;
        BigDecimal valorPL = null;
        String codigoTipo = null;
        BigDecimal valorFinTotalReceber = BigDecimal.ZERO;
        BigDecimal valorFinTotalPagar = BigDecimal.ZERO;
        BigDecimal valorCotaEmitir = BigDecimal.ZERO;
        BigDecimal valorCotaResgatar = BigDecimal.ZERO;
        String codigoTipoCota = null;
        BigDecimal valorAtivos = null;
        String[] id = null;

        /**
         * Verificando se PL = Quantidade de Cotas X Valor das Cotas
         */
        SecuritiesBalanceAccountingReportV04 mensagem = doc.getSctiesBalAcctgRpt();

        List<AggregateBalanceInformation13> balForAcctLst = mensagem.getBalForAcct();

        //Capturando a quantidade de cotas
        for (AggregateBalanceInformation13 listaFundos : balForAcctLst) {

            //Capturando a identificação do ativo
            Identificador identifica = new Identificador();
            SecurityIdentification14 listaIdentificadoresAtivo = listaFundos.getFinInstrmId();
            id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

            // Recuperando a qtd. de cotas
            try {
                quantidadeCotas = listaFundos.getAggtBal().getQty().getQty().
                        getQty().getUnit();
            } catch (NullPointerException e) {
                String errMsg = String.format(
                        "Não foi informada a Quantidade de Cotas para" + "ativo de código %s = %s", id[1], id[0]);
                log.debug(errMsg);
                errors.add(errMsg);
            }

            // Recuperando o valor das cotas
            List<PriceInformation5> pricDtlsLst = listaFundos.getPricDtls();

            for (PriceInformation5 pricDtls : pricDtlsLst) {
                TypeOfPrice11Code code = pricDtls.getTp().getCd();
                if (TypeOfPrice11Code.NAVL.equals(code)) {
                    valorCotas = pricDtls.getVal().getAmt().getValue();
                    break;
                }
            }

            // Recuperando o PL
            BigDecimal patrimLiq = BigDecimal.ZERO;
            try {
                patrimLiq = doc.getSctiesBalAcctgRpt().getAcctBaseCcyTtlAmts().
                        getTtlHldgsValOfStmt().getAmt().getValue();
            } catch (NullPointerException e) {
                String errMsg = "PL do fundo não foi encontrado para " + id[1] + " igual a " + id[0]
                        + ". <BsnsMsg><SctiesBalAcctgRpt><AcctBaseCcyTtlAmts>" + "<TtlHldgsValOfStmt><Amt>";
                log.debug(errMsg);
                errors.add(errMsg);
            }

            BigDecimal plCalculado = quantidadeCotas.multiply(valorCotas).
                    setScale(patrimLiq.scale(), RoundingMode.DOWN);
            BigDecimal vlrCotaCalculado = patrimLiq.divide(quantidadeCotas, valorCotas.scale(), RoundingMode.DOWN);
            BigDecimal qtdCotasCalculado = patrimLiq.divide(valorCotas, quantidadeCotas.scale(), RoundingMode.DOWN);
            BigDecimal qtdCotasCalculadoSemTrunc10 = patrimLiq.divide(valorCotas, 10, RoundingMode.DOWN);
            log.debug("qtdCotasCalculadoSemTrunc={}", qtdCotasCalculadoSemTrunc10);

            log.debug(String.format("\npl=%s; plCalc=%s" + "\ncota=%s; cotaCalc=%s" + "\nqtdCotas=%s; qtdCotasCalc=%s",
                    patrimLiq, plCalculado, valorCotas, vlrCotaCalculado, quantidadeCotas, qtdCotasCalculado));

            //            if ((patrimLiq.compareTo(plCalculado) != 0)
            //                    && (valorCotas.compareTo(vlrCotaCalculado) != 0)
            //                    && (quantidadeCotas.compareTo(qtdCotasCalculado) != 0)) {

            if (patrimLiq.subtract(plCalculado).abs().intValue() >= MARGEM_ERR) {
                String errMsg = "Valor do PL (" + patrimLiq + ") não é compatível com a " + "Quantidade de Cotas ("
                        + quantidadeCotas + ") e o Valor das Cotas (" + valorCotas + "). Verificar ativo de código "
                        + id[1] + " igual a " + id[0] + ". <BsnsMsg><SctiesBalAcctgRpt><AcctBaseCcyTtlAmts>"
                        + "<TtlHldgsValOfStmt><Amt>";
                log.debug(errMsg);
                errors.add(errMsg);
            }

            if (true) {
                return errors;
            }

            // TODO Verificar se o código abaixo faz sentido
            /**
             * Verificando se PL = Valor dos Ativos + Valores a Receber – Valores a Pagar – Cotas a
             * Emitir – Cotas a Resgatar.
             */
            //Capturando Valores a Receber e a Pagar
            List<SubBalanceInformation6> balBrkdwnLst = listaFundos.getBalBrkdwn();

            for (SubBalanceInformation6 listaPatrimonioFundo : balBrkdwnLst) {
                try {

                    codigoTipo = listaPatrimonioFundo.getSubBalTp().getPrtry().getId();
                    //Capturando Valores a Receber, se for igual a RECE
                    if (codigoTipo.equals("RECE")) {
                        valorFinTotalReceber = listaPatrimonioFundo.getQty().getQty().getFaceAmt();
                        //log.debug("Valores a receber = {}", valorFinTotalReceber);
                    }
                    //Capturando Valores a Pagar, se for igual a PAYA
                    if (codigoTipo.equals("PAYA")) {
                        valorFinTotalPagar = listaPatrimonioFundo.getQty().getQty().getFaceAmt();
                        //log.debug("Valores a pagar = {}", valorFinTotalPagar);
                    }
                } catch (NullPointerException e) {

                }
            }

            //Capturando Cotas a Emitir e a Receber
            for (SubBalanceInformation6 listaPatrimonioFundo2 : balBrkdwnLst) {
                List<AdditionalBalanceInformation6> addtlBalBrkdwnDtlsLst = listaPatrimonioFundo2.
                        getAddtlBalBrkdwnDtls();

                for (AdditionalBalanceInformation6 listaInfAdic : addtlBalBrkdwnDtlsLst) {

                    try {
                        codigoTipo = listaPatrimonioFundo2.getSubBalTp().getCd().toString();
                        //Capturando Cotas a Emitir se for igual a PEND
                        if (codigoTipo.equals("PEND")) {
                            valorCotaEmitir = listaInfAdic.getQty().getQty().getFaceAmt();
                            //log.debug("Valor Cotas a Emitir = {}", valorCotaEmitir);
                        }
                        //Capturando Cotas a Resgatar se for igual a PENR
                        if (codigoTipo.equals("PENR")) {
                            valorCotaResgatar = listaInfAdic.getQty().getQty().getFaceAmt();
                            //log.debug("Valor Cotas a Resgatar = {}", valorCotaResgatar);
                        }
                    } catch (NullPointerException e) {
                        //log.debug("PEND = {}; PENR = {}", valorCotaEmitir, valorCotaResgatar);
                    }
                }
            }

            //Capturando o Valor dos Ativos
            valorAtivos = listaFundos.getAcctBaseCcyAmts().getHldgVal().getAmt().
                    getValue();
            //log.debug("Valor dos Ativos = {}", valorAtivos);
            //Capturando valor do PL
            try {
                valorPL = mensagem.getAcctBaseCcyTtlAmts().getTtlHldgsValOfStmt().getAmt().
                        getValue();
                //log.debug("Valor do PL = {}", valorPL);
            } catch (NullPointerException e) {
                log.debug("Não informou seção do Pl");
            }
        }

        //Conferindo se PL = Quantidade de Cotas X Valor das Cotas
        String err_template = "Valor do PL deve ser igual à Quantidade de cotas * Valor das Cotas +/- 500. "
                + "Foi informado Quantidade de cotas igual a %s, Valor da Cota: %s e Valor de PL igual a %s. "
                + "Verificar ativo de código %s igual a %s. "
                + "<BsnsMsg><SctiesBalAcctgRpt><AcctBaseCcyTtlAmts><TtlHldgsValOfStmt><Amt>";
        try {
            if (valorPL.compareTo(quantidadeCotas.multiply(valorCotas).subtract(BigDecimal.valueOf(500))) == -1
                    || valorPL.compareTo(quantidadeCotas.multiply(valorCotas).add(BigDecimal.valueOf(500))) == 1) {
                errors.add(String.format(err_template, quantidadeCotas, valorCotas, valorPL, id[1], id[0]));
            }
        } catch (NullPointerException e) {
            errors.add(String.format(err_template, quantidadeCotas, valorCotas, valorPL, id[1], id[0]));
        }
        //Conferindo se PL  = Valor dos Ativos + Valores a Receber
        //– Valores a Pagar – Cotas a Emitir – Cotas a Resgatar.
        BigDecimal valorCompara = null;
        try {
            valorCompara = valorAtivos.add(valorFinTotalReceber);
            valorCompara = valorCompara.subtract(valorFinTotalPagar);
            valorCompara = valorCompara.subtract(valorCotaEmitir);
            valorCompara = valorCompara.subtract(valorCotaResgatar);
            //log.debug("Valor de comparação igual a {}", valorCompara);

            if (valorPL.compareTo(valorCompara.subtract(BigDecimal.valueOf(MARGEM_ERR))) == -1 || valorPL.compareTo(
                    valorCompara.add(BigDecimal.valueOf(MARGEM_ERR))) == 1) {
                errors.add("Valor de Pl do Fundo deve ser igual a : Valor dos Ativos + Valores a Receber"
                        + " - Valores a Pagar - Cotas a Receber - Cotas a Emitir com margem de 500. Foi informado PL "
                        + "do Fundo igual a " + valorPL + ", valor dos Ativos igual a " + valorAtivos + ", "
                        + "Valores a Receber igual a " + valorFinTotalReceber + ", Valores a Pagar" + " igual a "
                        + valorFinTotalPagar + ", Cotas a Emitir igual a " + valorCotaEmitir + ""
                        + ", e Cotas a Resgatar igual a " + valorCotaResgatar + ". <AcctBaseCcyTtlAmts>"
                        + "<TtlHldgsValOfStmt><Amt>");
            }
        } catch (NullPointerException e) {
        }
        return errors;
    }
}
