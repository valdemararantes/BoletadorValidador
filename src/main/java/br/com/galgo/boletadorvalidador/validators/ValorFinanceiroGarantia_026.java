/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.BalanceForAccountUtils;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AdditionalBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceInformation5;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceAccountingReportV04;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceType12Code;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceInformation6;
import java.math.BigDecimal;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Valor Financeiro Garantia não vinculada as margens das clearings – (PU de
 * Posição * Quantidade Garantia) / Lote”
 *
 * @author joyce.oliveira
 */
@ValidatorType
class ValorFinanceiroGarantia_026 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(ValorFinanceiroGarantia_026.class);
    private List<String> errors = Lists.newArrayList();

    @Override
    public List<String> validate(Document doc) {
        log.debug("Início do teste Valor Financeiro Garantia");

        errors = Lists.newArrayList();

        /*
         * Valor Financeiro total disponível = Quantidade Informada como
         * disponível * Valor da Cota
         */
        SecuritiesBalanceAccountingReportV04 mensagem = doc.getSctiesBalAcctgRpt();

        List<AggregateBalanceInformation13> balForAcctLst = mensagem.getBalForAcct();

        // Percorrendo cada fundo
        for (AggregateBalanceInformation13 balForAcct : balForAcctLst) {

            BigDecimal valorGarantia = null;
            BigDecimal valorTotalGarantia = null;

            //Capturando Valor da Cota
            BalanceForAccountUtils balForAcctUtils = new BalanceForAccountUtils(balForAcct);
            BigDecimal valorCotas = balForAcctUtils.getValorCota();

            List<SubBalanceInformation6> balBrkdwnLst = balForAcct.getBalBrkdwn();
            boolean isValorGarantiaDefined = false;
            for (SubBalanceInformation6 balBrkdwn : balBrkdwnLst) {

                //Capturando Quantidade Informada como Garantia
                SecuritiesBalanceType12Code subBalTp_cd = balBrkdwn.getSubBalTp().getCd();
//                if (SecuritiesBalanceType12Code.COLO.equals(subBalTp_cd)
//                        || SecuritiesBalanceType12Code.COLI.equals(subBalTp_cd)) {
//                    valorGarantia = balBrkdwn.getQty().getQty().getUnit();
//                }

                if (SecuritiesBalanceType12Code.COLO.equals(subBalTp_cd)
                        || SecuritiesBalanceType12Code.COLI.equals(subBalTp_cd)) {

                    isValorGarantiaDefined = true;

                    //Capturando Valor Financeiro Total Garantia
                    List<AdditionalBalanceInformation6> addtlBalBrkdwnDtlsLst = balBrkdwn.
                            getAddtlBalBrkdwnDtls();

                    try {
                        valorGarantia = balBrkdwn.getQty().getQty().getUnit();
                        if (addtlBalBrkdwnDtlsLst.size() > 1) {
                            String errMsg = "Para Valor Financeiro Garantia, representado "
                                    + "pelo código COLO ou COLI, a tag <addtlBalBrkdwnDtls> "
                                    + "deve ser informada apenas uma vez com o Valor "
                                    + "Financeiro Total Disponível.";
                            log.debug(errMsg);
                            errors.add(errMsg);
                        } else {
                            valorTotalGarantia = addtlBalBrkdwnDtlsLst.get(0).getQty().getQty().
                                    getFaceAmt();
                        }
                    } catch (NullPointerException e) {
                        log.debug("Não informou o Total Garantia");
                    }
                }
            }

            if (!isValorGarantiaDefined) {
                continue;
            }

            //Verificando se Valor financeiro total Disponível =
            //Valor Disponível * Valor Cota
            if (valorTotalGarantia == null
                    || valorCotas == null
                    || valorGarantia == null
                    || valorTotalGarantia.compareTo(valorCotas.multiply(valorGarantia)) != 0) {
                String errMsg = String.format("Valor Financeiro Total Garantia (%s), "
                        + "identificado pelo código COLO ou COLI, "
                        + "deve ser igual à Quantidade Informada como Garantia (%s) mutiplicada "
                        + "pelo Valor da cota (%s). <BalForAcct><BalBrkdwn><SubBalTp><Cd>",
                        valorTotalGarantia, valorGarantia, valorCotas);
                log.debug(errMsg);
                errors.add(errMsg);
            }
        }

        return errors;
    }
}
