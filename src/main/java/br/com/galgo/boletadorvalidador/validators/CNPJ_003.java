/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.GenericIdentification13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Intermediary21;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesAccount11;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Código CNPJ
 * <p/>
 * Preencher com o CNPJ da Instituição usar somente números
 * <p/>
 * O campo identificação <Id> deverá ser preenchido somente com números. O campo Emissor <Issr>
 * deverá ser preenchido com Receita Federal O campo Nome <SchmeNm> deverá ser preenchido com CNPJ
 *
 * @author joyce.oliveira
 */
@ValidatorType
class CNPJ_003 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(CNPJ_003.class);
    private List<String> errors;

    public List<String> validate(Document doc) {
        log.debug("Início do teste de CNPJ");

        errors = Lists.newArrayList();

        /**
         * Verificar CNPJ da sessão AccountOwner - Administrador
         * O preenchimento desta subseção é obrigatório no marcado brasileiro
         */
        try {
            //CNPJ deve possuir apenas dígitos
            String cnpjAcctOwnr = doc.getSctiesBalAcctgRpt().getAcctOwnr().getPrtryId().getId();
            verificaDigitos(cnpjAcctOwnr, "Administrador.<SctiesBalAcctgRpt><AcctOwnr>");

            //Verifica se emissor
            String emissorAcctOwner = doc.getSctiesBalAcctgRpt().getAcctOwnr().getPrtryId().getIssr();
            emissorAcctOwner = StringUtils.trimToEmpty(emissorAcctOwner);
            verificaEmmissorReceitaFederal(emissorAcctOwner, cnpjAcctOwnr, "<SctiesBalAcctgRpt><AcctOwnr>");

            //Verifica Tipo
            String tipoAcctOwnr = doc.getSctiesBalAcctgRpt().getAcctOwnr().getPrtryId().getSchmeNm();
            tipoAcctOwnr = StringUtils.trimToEmpty(tipoAcctOwnr);
            verificaTipo(tipoAcctOwnr, cnpjAcctOwnr, "<SctiesBalAcctgRpt><AcctOwnr>");
        } catch (NullPointerException e) {
            errors.add("Nao foi informado CNPJ do Administrador.<SctiesBalAcctgRpt><AcctOwnr>");
        }

        /**
         * Verificar o CNPj da sessão AccountService - Gestor Preenchimento da subseção é
         * obrigatória no mercado brasileiro
         */
        String cnpjAcctSrvc = null;

        try {

            cnpjAcctSrvc = doc.getSctiesBalAcctgRpt().getAcctSvcr().getPrtryId().getId();
            verificaDigitos(cnpjAcctSrvc, "Gestor. <SctiesBalAcctgRpt><AcctSvcr>");

            String emissorAcctSrvc = doc.getSctiesBalAcctgRpt().getAcctSvcr().getPrtryId().getIssr();
            verificaEmmissorReceitaFederal(emissorAcctSrvc, cnpjAcctSrvc, "<SctiesBalAcctgRpt><AcctSvcr>");

            String tipoAcctSrvc = doc.getSctiesBalAcctgRpt().getAcctSvcr().getPrtryId().getSchmeNm();
            verificaTipo(tipoAcctSrvc, cnpjAcctSrvc, "<SctiesBalAcctgRpt><AcctSvcr>");
        } catch (NullPointerException e) {
            errors.add("Nao foi informado CNPJ do Gestor.<SctiesBalAcctgRpt><AcctSvcr>");
        }

        verificaCNPJCustodiante(doc);

        /**
         * Verificar CNPJs na sessão IntermediaryInformations que permite repetição, referente a
         * Intermediários ( distribuidores, consultores, etc) pode informar até 10 intermediários
         *
         * No mercado brasileiro não é obrigatório informar
         *
         */
        List<Intermediary21> IntrmyInf = doc.getSctiesBalAcctgRpt().getIntrmyInf();

        String cnpjOutros;
        String emissorOutros;
        String tipoOutros;

        for (Intermediary21 cnpjIntrmyInf : IntrmyInf) {
            try {
                cnpjOutros = cnpjIntrmyInf.getId().getPrtryId().getId();
                verificaDigitos(cnpjOutros, "Intermediários. <SctiesBalAcctgRpt><IntrmyInf>");
                emissorOutros = cnpjIntrmyInf.getId().getPrtryId().getIssr();
                verificaEmmissorReceitaFederal(emissorOutros, cnpjOutros, "<SctiesBalAcctgRpt><IntrmyInf>");
                tipoOutros = cnpjIntrmyInf.getId().getPrtryId().getSchmeNm();
                verificaTipo(tipoOutros, cnpjOutros, "<SctiesBalAcctgRpt><IntrmyInf>");
            } catch (NullPointerException e) {
                log.debug("Não informou CNPJ da Subseção Intermediários. " + "<SctiesBalAcctgRpt><IntrmyInf>");
            }
        }

        /**
         * Verificando CNPJ da sessão Balance For Account, este campo só é validado caso o tipo seja
         * igual a CNPJ, já que CNPJ não é obrigatório para identificar um fundo
         */
        List<AggregateBalanceInformation13> balForAcctList = doc.
                getSctiesBalAcctgRpt().getBalForAcct();

        String cnpjFundo;
        String tipoIdFundo;
        String emissorIdFundo;

        for (AggregateBalanceInformation13 listaBalFotAcct : balForAcctList) {
            try {
                List<OtherIdentification1> listaCNPJ;

                listaCNPJ = listaBalFotAcct.getFinInstrmId().getOthrId();

                for (OtherIdentification1 listaIdFundo : listaCNPJ) {
                    tipoIdFundo = listaIdFundo.getTp().getCd();
                    cnpjFundo = listaIdFundo.getId();
                    tipoIdFundo = StringUtils.trimToEmpty(tipoIdFundo);
                    /**
                     * Se fundo possuir identificação por CNPJ, deve validar se possui apenas
                     * dígitos
                     */
                    if (tipoIdFundo.equals("CNPJ")) {
                        verificaDigitos(cnpjFundo, " Fundo. <SctiesBalAcctgRpt>" + "<BalForAcct><FinInstrmId>");
                    }
                }
            } catch (NullPointerException e) {
                e.getMessage();
            }
        }

        /**
         * Verificando CNPJ na sessão SubAccountDetails
         */
        List<SubAccountIdentification16> subAcctDtls = doc.getSctiesBalAcctgRpt().getSubAcctDtls();
        String cnpjAtivo;
        String emissorIdAtivo;
        String tipoIdAtivo;

        for (SubAccountIdentification16 listaSubAcctDtls : subAcctDtls) {
            try {
                cnpjAtivo = listaSubAcctDtls.getAcctOwnr().getPrtryId().getId();
                verificaDigitos(cnpjAtivo, "Ativo. <SctiesBalAcctgRpt><SubAcctDtls><AcctOwnr>");
                emissorIdAtivo = listaSubAcctDtls.getAcctOwnr().getPrtryId().getIssr();
                tipoIdAtivo = listaSubAcctDtls.getAcctOwnr().getPrtryId().getSchmeNm();
                verificaEmmissorReceitaFederal(emissorIdAtivo, cnpjAtivo, "<SctiesBalAcctgRpt><SubAcctDtls><AcctOwnr>");
                verificaTipo(tipoIdAtivo, cnpjAtivo, "<SctiesBalAcctgRpt><SubAcctDtls><AcctOwnr>");
            } catch (NullPointerException e) {
                //log.debug("Ativo não identificado por CNPJ");
            }
        }
        return errors;
    }

    /**
     * Verifica o CNPJ do Custodiante, SafeKeepingAccount
     * O campo identificação (&lt;Id&gt;) do custodiante, deverá ser preenchido somente com números.
     * O campo identificação dentro da identificação proprietária deverá ser preenchido com o CNPJ.
     * O campo Emissor (&lt;Issr&gt;) deverá ser preenchido com Receita Federal do Brasil.
     * O campo nome do custodiante <Nm> deverá ser
     * preenchido com o nome do custodiante (máximo de 70 caracteres);
     *
     * @param doc
     */
    private void verificaCNPJCustodiante(Document doc) {
        try {
            final SecuritiesAccount11 sfkpgAcct = doc.getSctiesBalAcctgRpt().getSfkpgAcct();
            String cnpj = sfkpgAcct.getId();
            /*
                ( Ver Manual - Arquivo de Posição 5.0)
                5.1	IDENTIFICAÇÃO
                Seção/Subseção:Custódia (SafekeepingAccount)
                Campo: Identificação
                Domínio: Livre
                Definição: Identificação
                Preenchimento: Obrigatório. Formato numérico de até 35 caracteres.
                               Preencher com o CNPJ da Instituição. Usar somente números
                Tag: <Id></Id>
             */
            verificaDigitos(cnpj, "Custodiante. <SctiesBalAcctgRpt><SfkpgAcct>");

            /*
            CONVENCIONOU-SE A UTILIZAÇÃO DA IDENTIFICAÇÃO PROPRIETÁRIA PARA INFORME DO CNPJ
            DO CUSTODIANTE CONFORME EXEMPLO ABAIXO:

            IDENTIFICAÇÃO PROPRIETÁRIA
            IDENTIFICAÇÃO: CNPJ
            EMISSOR: Receita Federal do Brasil
            */
            final GenericIdentification13 prtry = sfkpgAcct.getTp().getPrtry();
            final String emissor = prtry.getIssr();
            verificaEmmissorReceitaFederal(emissor, cnpj, "<SctiesBalAcctgRpt><SfkpgAcct>");
            final String idTipo = prtry.getId();
            verificaTipo(idTipo, cnpj, "<SctiesBalAcctgRpt><SfkpgAcct><Tp>");

        } catch (NullPointerException e) {
            log.debug("Não informou corretamente o CNPJ do Custodiante. <SctiesBalAcctgRpt><SfkpgAcct>");
        }
    }

    /**
     * Verifica se tag <Id> do CNPJ possui apenas numeros
     */
    private void verificaDigitos(String cnpj, String idSecao) {
        //log.debug("Verificando se CNPJ {} possui apenas dígitos ", cnpj);
        if (!StringUtils.isNumeric(cnpj)) {
            errors.add(
                    "CNPJ deve conter apenas números. Foi informado : " + cnpj + ". " + "Verificar CNPJ referente ao "
                            + idSecao);
        }
    }

    /**
     * Verifica se emissor igual a Receita Federal do Brasil
     */
    private void verificaEmmissorReceitaFederal(String emissorCnpj, String cnpj, String idSecao) {
        //log.debug("Verificando se amissor do igual a Receita Federal do Brasil ");
        if (!StringUtils.equals("Receita Federal do Brasil", emissorCnpj)) {
            errors.add("Emissor deve ser igual a Receita Federal do Brasil, " + "foi informado: " + emissorCnpj
                    + ". Referente ao CNPJ igual a " + cnpj + ". " + idSecao);
        }
    }

    /**
     * Verifica se tipo igual a CNPJ
     */
    private void verificaTipo(String tipoCNPJ, String cnpj, String idSecao) {
        if (!StringUtils.equals("CNPJ", tipoCNPJ)) {
            errors.
                    add("O campo id proprietário deve ser preenchido com a String 'CNPJ', foi informado: " + tipoCNPJ
                            + ". Referente ao CNPJ igual a " + cnpj + ". " + idSecao);
        }
    }
}
