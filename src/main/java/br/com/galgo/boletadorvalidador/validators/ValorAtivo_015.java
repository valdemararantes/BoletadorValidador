/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.BalanceForAccountUtils;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.AmountAndDirection6;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceInformation5;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesAccount14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Valor das Posições disponíveis + Posições doadas - Posições vendidas + ajustes Mercado Futuro /
 * Net Swap / Prêmio de Opções e demais derivativos na “Data Posição”.
 *
 * @author joyce.oliveira
 */
@ValidatorType
class ValorAtivo_015 implements Validator_ {

    private static final BigDecimal BIGDECIMAL_MINUS_ONE = new BigDecimal(-1);
    private static final Logger log = LoggerFactory.getLogger(
            ValorAtivo_015.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste de Valor do Ativo");

        errors = Lists.newArrayList();

        /**
         * Verificar se: Valor financeiro
         * <Amt><HldgVal><AcctBaseCcyAmts><BalForSubAcct><SubAcctDtls>
         * = preço unitário <Amt><Val><PricDtls><BalForSubAcct><SubAcctDtls>
         * X quantidade de cotas <Unit><Qty><Qty><Qty><AggtBal><BalForSubAcct><SubAcctDtls>
         */
        String codigoPreco = null;
        BigDecimal valorFinanceiro = null;
        BigDecimal quantidadeCotas = null;
        BigDecimal precoUnitario = null;
        // Variável para totalizar os ativos
        BigDecimal valorTotalAtivos = BigDecimal.ZERO;

        List<AggregateBalanceInformation13> balForAcctList = doc.getSctiesBalAcctgRpt().
                getBalForAcct();
         BalanceForAccountUtils balForAcctUtils = new BalanceForAccountUtils(balForAcctList.get(0));
         String fundoId = balForAcctUtils.getIdAsString();

        /*
         * //doc/SctiesBalAcctgRpt/SubAcctDtls[]
         *
         * Esta seção contém todos os ativos da carteira.
         */
        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();
        for (SubAccountIdentification16 subAcctDtls : subAcctDtlsLst) {

            // Identificação da conta de Custódia para os ativos que serão informados
            SecuritiesAccount14 sfkpgAcct = subAcctDtls.getSfkpgAcct();
            log.debug("Identificação da conta de Custódia: {}", getObjAsJSONString(sfkpgAcct));

            /* //doc/SctiesBalAcctgRpt/SubAcctDtls[]/BalForSubAcct[] */
            List<AggregateBalanceInformation13> balForSubAcctLst = subAcctDtls.getBalForSubAcct();

            // Quantidade de ativos com valores
            int qtdAtivosComValor = 0;

            if (balForSubAcctLst != null) {
                log.info("Quantidade de ativos: {}", balForSubAcctLst.size());
            } else {
                log.info("Nenhum ativo encontrado");
            }

            /*
             * Percorrendo cada ativo
             *         //doc/SctiesBalAcctgRpt/SubAcctDtls[]/BalForSubAcct[]
             */
            for (AggregateBalanceInformation13 balForSubAcct : balForSubAcctLst) {

                valorFinanceiro = BigDecimal.ZERO;

                //Capturando Valor Financeiro Informado
                // //doc/SctiesBalAcctgRpt/SubAcctDtls[]/BalForSubAcct[]/AcctBaseCcyAmts/HldgVal/Amt
                try {
                    AmountAndDirection6 hldgVal = balForSubAcct.getAcctBaseCcyAmts().getHldgVal();
                    valorFinanceiro = hldgVal.getAmt().getValue();
                    if (!hldgVal.isSgn()) {
                        valorFinanceiro = valorFinanceiro.multiply(BIGDECIMAL_MINUS_ONE);
                    }
                    qtdAtivosComValor++;
                } catch (Exception e) {
                    log.info(e.getMessage());
                }

                // Totalizando os ativos
                valorTotalAtivos = valorTotalAtivos.add(valorFinanceiro);

                // Gerando um log com valor da cota e quantidade de cotas
                //log.debug("valorFinanceiro={}", valorFinanceiro);
                //qtdCotasXValorCota(balForSubAcct);
            }

            log.info("Quantidade de ativos com valor: {}", qtdAtivosComValor);
            log.info("Valor totalizado de ativos: {}", valorTotalAtivos);

//            if (valorFinanceiro.compareTo(precoUnitario.multiply(quantidadeCotas)) != 0) {
//                errors.add("Valor do Ativo deve ser igual a (quantidade de cotas) *"
//                        + " (preço unitário). Foi informado Valor Financeiro igual a "
//                        + valorFinanceiro + ", valor da cota igual a " + quantidadeCotas
//                        + ", e preço unitário igual a " + precoUnitario);
//            }
        }

        return errors;
    }

    private String getObjAsJSONString(Object obj) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            StringWriter writer = new StringWriter();
            ObjectWriter writerWithDefaultPrettyPrinter = mapper.writer();
//                ObjectWriter writerWithDefaultPrettyPrinter = mapper.
//                        writerWithDefaultPrettyPrinter();
            writerWithDefaultPrettyPrinter.writeValue(writer, obj);
            return writer.getBuffer().toString();
        } catch (Exception e) {
            log.error(null, e);
            return null;
        }
    }

    /**
     * No momento está apenas logando o valor da cota, a qtd. de cotas e sua multplicação
     *
     * @param balForSubAcct
     */
    private void qtdCotasXValorCota(AggregateBalanceInformation13 balForSubAcct) {
        try {
            BigDecimal precoUnitario = null;
            BigDecimal qtdCotas = null;

            /* //doc/SctiesBalAcctgRpt/SubAcctDtls[]/BalForSubAcct[]/PricDtls[]
             * Capturando Quantidade de cotas
             * balForSubAcct/AggtBal/Qty/Qty/Qty/Unit
             */
            try {
                qtdCotas = balForSubAcct.getAggtBal().getQty().getQty().getQty().getUnit();
            } catch (NullPointerException e) {
            }

            List<PriceInformation5> pricDtlsLst = balForSubAcct.getPricDtls();
            int pricDtlsLstCounter = 0;
            for (PriceInformation5 listaPrecos : pricDtlsLst) {
                pricDtlsLstCounter++;
                //Capturando preço unitário, verificar o valor que estiver na seção com
                //PriceDetails de Código Igual a MRKT
                String codigoPreco = null;
                try {
                    codigoPreco = listaPrecos.getTp().getCd().toString();
                    if (codigoPreco.equals("MRKT")) {
                        try {
                            precoUnitario = listaPrecos.getVal().getAmt().getValue();
                            log.debug("precoUnitario={}; qtdCotas={}", precoUnitario, qtdCotas);
                        } catch (NullPointerException e) {
                        }
                    }
                } catch (NullPointerException e) {
                    log.info("codigoPreco não encontrado no XML (pricDtlsLst[{}])",
                            pricDtlsLstCounter);
                }
            }
        } catch (Exception e) {
            log.error(null, e);
        }
    }
}
