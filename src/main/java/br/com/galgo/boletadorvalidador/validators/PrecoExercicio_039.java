/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Preço de Exercício (StrikePrice)
 * Verificar, é obrigatório para os ativos do tipo:
 * Opções de ações;
 * Opções de Derivativos;
 * Opções de Moedas;
 * Opções Flexíveis
 *
 * Deve informar na tag ValTp o tipo PARV
 *
 * @author joyce.oliveira
 */
@ValidatorType
class PrecoExercicio_039 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            PrecoExercicio_039.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Preço do Exercício");

        errors = Lists.newArrayList();

        boolean verificaPrecoExercicio = false;
        String tipoAtivo = null;
        String codigoTipoCVM = null;
        String tipoValor = null;
        BigDecimal valoFinanceiro = null;

        List<String> listaCodigosCVM = new ArrayList<>();
        //Códigos CVM dos tipos de ativos que devem informar Preço Exercicio
        listaCodigosCVM.add("39");
        listaCodigosCVM.add("40");

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 listaDetalheAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivos.
                    getBalForSubAcct();

            //Verificar se tipo do ativo obriga Preço Exercício
            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {
                List<OtherIdentification1> othrIdLst = listaAtivos.getFinInstrmId().
                        getOthrId();
                verificaPrecoExercicio = false;

                 //Capturando a identificação do ativo
	Identificador identifica = new Identificador();
	SecurityIdentification14 listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
	String[] id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                try {

                    for (OtherIdentification1 listaIdAtivos : othrIdLst) {
                        tipoAtivo = listaIdAtivos.getTp().getPrtry();
                        //log.debug("Tipo Identificacao: {}", tipoAtivo);
                        if (tipoAtivo.equals("CVM CDA 3.0 Tabela B")) {
                            //log.debug("Tipo CVM do ativo = {}", codigoTipoCVM);
                            codigoTipoCVM = listaIdAtivos.getId();

                            if (listaCodigosCVM.contains(codigoTipoCVM)) {
                                verificaPrecoExercicio = true;
                                break;
                            }
                        }
                    }
                } catch (NullPointerException e) {
                    //log.debug("Não tem <Cd>");
                }
                if (verificaPrecoExercicio) {
                    //Verificar se informou PARV
                    try {
                        tipoValor = listaAtivos.getFinInstrmAttrbts().getStrkPric().getTp().
                                getValTp().toString();
                        if (!tipoValor.equals("PARV")) {
                            errors.
                                    add("Para Preço de Exercício é obrigatório informar código PARV, "
                                    + "foi informado " + tipoValor
                                    + ". Verificar ativo do tipo CVM "
                                    + "igual a " + codigoTipoCVM + ""
                                    + "Verificar ativo de código "+id[1]+" igual a "+ id[0]+". "
                                    + "<BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls><BalForSubAcct>"
                                    + "<FinInstrmAttrbts><StrkPric><Tp><ValTp>");
                        }
                    } catch (NullPointerException e) {
                        errors.add("Obrigatório informar Preço de Exercício "
                                + "para ativo de código CVM igual a " + codigoTipoCVM + ". "
                                + "Verificar ativo de código "+id[1]+" igual a "+ id[0]+". "
                                + "<BsnsMsg><SctiesBalAcctgRpt><SubAcctDtls><BalForSubAcct>"
                                + "<FinInstrmAttrbts><StrkPric>");
                    }
                }
            }
        }
        return errors;
    }
}
