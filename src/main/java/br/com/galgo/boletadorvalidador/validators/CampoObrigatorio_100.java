/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.BalanceForAccountUtils;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Verificar se é informado um dos seguintes identificadores:
 * 1.	Código ISIN, se informado 2.	Código STI, se informado
 * 3.	CNPJ, se informado 4.	Sigla da Carteira
 *
 * @author joyce.oliveira
 */
@ValidatorType
class CampoObrigatorio_100 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(CampoObrigatorio_100.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Campos Obrigatórios");

        errors = Lists.newArrayList();

        List<AggregateBalanceInformation13> balForAcctList = doc.getSctiesBalAcctgRpt().
                getBalForAcct();

        // Percorrendo os fundos
        for (AggregateBalanceInformation13 balForAcct : balForAcctList) {

            BalanceForAccountUtils balForAcctUtils = new BalanceForAccountUtils(balForAcct);
            List<BalanceForAccountUtils.Id> idList = balForAcctUtils.getIdList();

            // Se fundo
            for (BalanceForAccountUtils.Id id : idList) {
                if ("CNPJ".equals(id.code)
                        || "ISIN".equals(id.code)
                        || "Código STI".equals(id.code)) {
                    return errors;
                }
            }

            // Se carteira
            if (!idList.isEmpty()) {
                return errors;
            }

            errors.add("Validação do Sistema Galgo: - É obrigatório informar Código ISIN"
                    + " ou CNPJ ou Código STI ou Sigla "
                    + "da Carteira para identificar o ativo.");

        } // Fim dos fundos

        return errors;

//        //Verificando identificador do Fundo
//        List<AggregateBalanceInformation13> balForAcctLst = doc.getSctiesBalAcctgRpt().getBalForAcct();
//
//        for(AggregateBalanceInformation13 listaFundos : balForAcctLst){
//            SecurityIdentification14 identificadorFundo = listaFundos.getFinInstrmId();
//
//            String codFundo[] = new String[2];
//            codFundo = retornaIdentificador(identificadorFundo);
//            //log.debug("Identificador do Fundo igual a "+ codFundo[1]+" - " + codFundo[0]);
//            if(codFundo[0] == null){
//                log.debug("É obrigatório informar Código ISIN ou CNPJ ou Código STI ou Sigla "
//                        + "da Carteira para identificar o fundo.");
//            }
//        }
//        //Verificando identificador do Ativo
//        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().getSubAcctDtls();
//
//        for(SubAccountIdentification16 listaDetalheAtivo : subAcctDtlsLst){
//            List<AggregateBalanceInformation13> balForSubAcctLst = listaDetalheAtivo.getBalForSubAcct();
//
//            for(AggregateBalanceInformation13 listaAtivos: balForSubAcctLst){
//                SecurityIdentification14 secaoId = listaAtivos.getFinInstrmId();
//
//                String[] codAtivo = new String[2];
//
//                codAtivo = retornaIdentificador(secaoId);
//                //log.debug("Identificador do Ativo igual a "+ codAtivo[1]+" - " + codAtivo[0]);
//                if(codAtivo[0] == null){
//                errors.add("Validação do Sistema Galgo: - É obrigatório informar Código ISIN"
//                        + " ou CNPJ ou Código STI ou Sigla "
//                        + "da Carteira para identificar o ativo.");
//                }
//            }
//        }
//
//        return errors;
    }

    String[] retornaIdentificador(SecurityIdentification14 secaoId) {

        String identificador = null;
        String codigoIdentificador = null;
        String id[] = new String[2];

        //Se tiver ISIN, considera este identificador senão procura outro tipo
        identificador = secaoId.getISIN();

        if (identificador != null) {
            codigoIdentificador = "ISIN";
        } //Se não tiver ISIN, procura Código STI
        else {
            List<OtherIdentification1> othrIdLst = secaoId.getOthrId();

            for (OtherIdentification1 listaIds : othrIdLst) {

                if ("SistemaGalgo".equals(listaIds.getTp().getPrtry())) {
                    codigoIdentificador = "Código STI";
                    identificador = listaIds.getId();
                    break;
                } else {
                    if ("CNPJ".equals(listaIds.getTp().getCd())) {
                        codigoIdentificador = listaIds.getTp().getCd();
                        identificador = listaIds.getId();
                        break;
                    }
                }
                //TODO Incluir verificação de Sigla
            }

        }
        id[0] = identificador;
        id[1] = codigoIdentificador;

        return id;
    }
}
