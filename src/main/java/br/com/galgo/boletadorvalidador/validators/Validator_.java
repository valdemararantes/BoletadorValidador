/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import java.util.List;

/**
 *
 * @author valdemar.arantes
 */
interface Validator_ {

    /**
     * Valida o objeto retornando uma lista de String's com os erros.
     * Se a lista retornada estiver vazia então não foram encontrados erros.
     *
     * @param doc
     * @return Lista de erros encontrados
     */
    List<String> validate(Document doc);
}
