/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AdditionalBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.PriceInformation5;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceAccountingReportV04;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceInformation6;
import java.math.BigDecimal;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Valor Financeiro Disponível = Quantidade informada como disponível(AWAS) * valor da cota
 *
 * @author joyce.oliveira
 */
@ValidatorType
class ValorFinanceiroDisponivel_025 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            ValorFinanceiroDisponivel_025.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Valor Financeiro Disponível");

        errors = Lists.newArrayList();

        /**
         * Valor Financeiro total disponível = Quantidade Informada como disponível * Valor da Cota
         */
        BigDecimal valorCotas = null;
        String codigoTipo = null;
        BigDecimal valorDiponivel = null;
        BigDecimal valorTotalDisponivel = null;
        SecuritiesBalanceAccountingReportV04 mensagem = doc.getSctiesBalAcctgRpt();

        List<AggregateBalanceInformation13> balForAcctLst = mensagem.getBalForAcct();

        //Capturar valores para fundo
        for(AggregateBalanceInformation13 listaFundo: balForAcctLst){
            List<PriceInformation5> pricDtlsLst = listaFundo.getPricDtls();
            //Valor da Cota
            valorCotas = getValorCota(pricDtlsLst);

            //Valor Disponível

        }

        //Capturar valores para ativo



        return errors;
    }

    /**
     * Recebe uma subseção de Preço (PriceInformation5) e retorna o valor da cota
     * @param secaoPreco lista do tipo PriceInformation5
     * @return valor da cota encontrada
     */
    BigDecimal getValorCota(List<PriceInformation5> secaoPreco){
        BigDecimal valorCota = null;
        for(PriceInformation5 listaPreco : secaoPreco){
        valorCota = listaPreco.getVal().getAmt().getValue();
        }
        return valorCota;
    }

    /**
     *
     * Verifica se o Valor Financeiro Total Disponível, é igual ao Valor Disponível
     * multiplicado pelo Valor da Cota
     *
     * @param valorFinanceiroTotalDisponivel
     * @param valorDisponivel
     * @param valorCota
     *
     */
    void confereValorDisponivel(BigDecimal valorFinanceiroTotalDisponivel, BigDecimal valorDisponivel,
            BigDecimal valorCota){

        if(valorFinanceiroTotalDisponivel.compareTo(valorCota.multiply(valorDisponivel))!= 0){
             errors.
                            add("Valor Financeiro Total Disponível, identificado pelo código AWAS, "
                            + "deve ser igual a Quantidade Informada como Disponível mutiplicada"
                            + "pelo Valor da cota. Valor Financeiro Total Disponível "
                            + "informado igual a " + valorFinanceiroTotalDisponivel + ", quantidade "
                            + "informada como Disponível igual a " + valorDisponivel + ", e "
                            + "Valor da Cota igual a " + valorCota + ". "
                            + "<BalForAcct><BalBrkdwn><SubBalTp><Cd>");
        }
    }
}
