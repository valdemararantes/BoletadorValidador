/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
@ValidatorType
class Debentures_007 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            Debentures_007.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste de Debentures");

        errors = Lists.newArrayList();

        /**
         * Se possuir identificador Debentures, deve verificar: O campo identificação <Id> deverá
         * ser preenchido com o código da Debênture. O campo código <Cd> dentro de Tipo <Tp> deverá
         * ser preenchido com o código MIC do ambiente de negociação. Exemplo: BVMF.
         */
        /**
         * Verificar se é Debenture, através da tag Classificação Alternativa <AltrnClssfctn> dentro
         * da subseção Classificação do Ativo
         */
        List< SubAccountIdentification16> subAcctDtls = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();
        String tpAtivo;

        for (SubAccountIdentification16 ativo : subAcctDtls) {
            List<AggregateBalanceInformation13> balForSubAcct = null;
            balForSubAcct = ativo.getBalForSubAcct();
            String idTipoAtivo = null;
            String idDebenture = null;

            for (AggregateBalanceInformation13 balSubAcct : balForSubAcct) {
                try {
                    CodigoMIC_002 codMIC = new CodigoMIC_002();
                    String idCodDebenture = null;
                    List<OtherIdentification1> othrId = balSubAcct.getFinInstrmId().getOthrId();
                    /**
                     * Se encontrar algum OthrId com codigo MIC, salva o Id
                     */
                    for (OtherIdentification1 listaIdAtivo : othrId) {

                        String codTpAtivo = listaIdAtivo.getTp().getCd();
                        idCodDebenture = listaIdAtivo.getId();
                        if (codMIC.verificaIso10383(codTpAtivo)) {
                            idTipoAtivo = codTpAtivo;
                            idDebenture = idCodDebenture;

                        }
                    }
                    List<OtherIdentification1> othrId2 = balSubAcct.getFinInstrmId().getOthrId();

                    for (OtherIdentification1 listaIdAtivo2 : othrId2) {

                        //Capturando a identificação do ativo
                        Identificador identifica = new Identificador();
                        SecurityIdentification14 listaIdentificadoresAtivo = balSubAcct.
                                getFinInstrmId();
                        String[] idAtivo = identifica.
                                retornaIdentificador(listaIdentificadoresAtivo);

                        String codTpProprietarioAtivo = null;

                        try {
                            codTpProprietarioAtivo = listaIdAtivo2.getTp().getPrtry();
                            /**
                             * Se houve IdentificacaoProprietaria = CVM CDA 3.0 Tabela B, valor do
                             * Id = 193 e O valor da Variavel idTipoAtivo for diferente de null (que
                             * significa que encontrou código MIC), é uma debenture deve verificar
                             * quantidade de caracteres entre 6 e 12
                             *
                             * Código da Debênture fornecido pelo Sistema Nacional de Debênture
                             * ou pela BVMF, ou pelo ambiente de negociação.
                             *
                             */
                            if (codTpProprietarioAtivo.equals("CVM CDA 3.0 Tabela B")
                                    && idTipoAtivo != null) {
                                String id = listaIdAtivo2.getId();
                                if (id.equals("193")) {
                                    log.debug("Codigo da debenture igual a : {}", idDebenture);
                                    
                                    if (idDebenture.length() < 6 || idDebenture.length() > 12) {
                                        errors.add("Código da Debenture deve ter entre 6 e 12 "
                                                + "caracteres. Foi informado: " + idDebenture
                                                + ". Verificar ativo de código " + idAtivo[1]
                                                + " igual a " + idAtivo[0] + ".");
                                    }
                                }
                            }
                        } catch (NullPointerException e) {
                        }
                    }
                } catch (NullPointerException e) {
                }
            }
        }
        return errors;
    }
}