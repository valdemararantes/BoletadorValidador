package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AdditionalBalanceInformation6;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecuritiesBalanceAccountingReportV04;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubBalanceInformation6;
import java.math.BigDecimal;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Verificar se o código inserido for DIRT significa que o ativo é tributado portanto será
 * obrigatório informar algum número no valor de face.
 *
 * @author joyce.oliveira
 */
@ValidatorType
class Impostos_012 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            Impostos_012.class);
    private List<String> errors = Lists.newArrayList();

    public List<String> validate(Document doc) {
        log.debug("Início do teste Impostos");

        errors = Lists.newArrayList();

        String idAtivo = null;
        String verificaImposto = null;
        String codigoQuantidade = null;
        BigDecimal faceAmount = null;

        SecuritiesBalanceAccountingReportV04 estruturaInicial = doc.getSctiesBalAcctgRpt();
        List<SubBalanceInformation6> balBrkdwnLst;

        //Capturando a identificação do ativo
        Identificador identifica = new Identificador();
        SecurityIdentification14 listaIdentificadoresAtivo;
        String[] id;
        // listaIdentificadoresAtivo=listaAtivos.getFinInstrmId();
        //id=identifica.retornaIdentificador(listaIdentificadoresAtivo);

        //Validar se valores do fundo são tributáveis
        List<AggregateBalanceInformation13> balForAcctLst = estruturaInicial.getBalForAcct();

        for (AggregateBalanceInformation13 listaFundos : balForAcctLst) {
            balBrkdwnLst = listaFundos.getBalBrkdwn();

            for (SubBalanceInformation6 listaQuantidades : balBrkdwnLst) {
                List<AdditionalBalanceInformation6> addtlBalBrkdwnDtlsLst = listaQuantidades.
                        getAddtlBalBrkdwnDtls();

                for (AdditionalBalanceInformation6 listaInfAdicionais : addtlBalBrkdwnDtlsLst) {
                    try {
                        listaIdentificadoresAtivo = listaFundos.getFinInstrmId();
                        id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                        codigoQuantidade = listaInfAdicionais.getSubBalTp().getCd().toString();
                        if ("DIRT".equals(codigoQuantidade)) {
                            faceAmount = listaInfAdicionais.getQty().getQty().getFaceAmt();
                            //Se informou código DIRT é tributado, então deve informar FaceAmount
                            if (faceAmount == null) {
                                errors.add("Quando informado o código DIRT na subseção "
                                        + "AdditionalBalanceBreakdownDetails, é obrigstório utilizar "
                                        + "a tag <FaceAmt> para informar o valor. Verificar ativo de "
                                        + "código " + id[1] + " igual a " + id[0] + ". <BsnsMsg>"
                                        + "<SctiesBalAcctgRpt><BalForAcct><BalBrkdwn>"
                                        + "<AddtlBalBrkdwnDtls><Qty><Qty><FaceAmt>");
                            }
                        }
                    } catch (NullPointerException e) {
                    }

                    codigoQuantidade = null;
                    faceAmount = null;
                }
            }
        }

        //Validar se existem valores tributáveis nos ativos
        List<SubAccountIdentification16> subAcctDtlsLst = estruturaInicial.getSubAcctDtls();
        for (SubAccountIdentification16 listaInfAtivos : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = listaInfAtivos.getBalForSubAcct();
            for (AggregateBalanceInformation13 listaAtivos : balForSubAcctLst) {

                balBrkdwnLst = listaAtivos.getBalBrkdwn();

                for (SubBalanceInformation6 listaQuantidades : balBrkdwnLst) {
                    List<AdditionalBalanceInformation6> addtlBalBrkdwnDtlsLst = listaQuantidades.
                            getAddtlBalBrkdwnDtls();

                    for (AdditionalBalanceInformation6 listaInfAdicionais : addtlBalBrkdwnDtlsLst) {
                        try {
                            listaIdentificadoresAtivo = listaAtivos.getFinInstrmId();
                            id = identifica.retornaIdentificador(listaIdentificadoresAtivo);

                            codigoQuantidade = listaInfAdicionais.getSubBalTp().getCd().toString();
                            if ("DIRT".equals(codigoQuantidade)) {
                                faceAmount = listaInfAdicionais.getQty().getQty().getFaceAmt();
                                //Se informou código DIRT é tributado, então deve informar FaceAmount
                                if (faceAmount == null) {
                                    errors.add("Quando informado o código DIRT na subseção "
                                            + "AdditionalBalanceBreakdownDetails, é obrigstório utilizar "
                                            + "a tag <FaceAmt> para informar o valor. Verificar fundo de "
                                            + "código " + id[1] + " igual a " + id[0]
                                            + ". <BsnsMsg>"
                                            + "<SctiesBalAcctgRpt><SubAcctDtls><BalForSubAcct><BalBrkdwn>"
                                            + "<AddtlBalBrkdwnDtls><Qty><Qty><FaceAmt>");
                                }
                            }
                        } catch (NullPointerException e) {
                        }
                        codigoQuantidade = null;
                        faceAmount = null;
                    }
                }
            }
        }
        return errors;
    }
}
