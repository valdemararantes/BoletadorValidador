/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import br.com.galgo.boletadorvalidador.utils.Identificador;
import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.semt_003_001.AggregateBalanceInformation13;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import iso.std.iso._20022.tech.xsd.semt_003_001.OtherIdentification1;
import iso.std.iso._20022.tech.xsd.semt_003_001.SecurityIdentification14;
import iso.std.iso._20022.tech.xsd.semt_003_001.SubAccountIdentification16;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Quando a tag “SctiesBalAcctgRpt/BalForAcct” for informada mais que uma vez o erro
 * PA.0072 será retornado, pois deve existir somente um Fundo ou Carteira por
 * Informacao de Posição de Ativos.
 *
 * Quando a tag “FinInstrmId/OthrId/Tp/Prtry” ou “FinInstrmId/OthrId/Tp/Cd” for enviada
 * mais de uma vez com o mesmo tipo de Identificação (exemplo: “SistemaGalgo”) para
 * uma Posição de Ativos, o erro PA.0072 será retornado.
 *
 * @author joyce.oliveira
 */
@GalgoValidatorType
class CampoInvalido_105 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            CampoInvalido_105.class);
    private List<String> errors = Lists.newArrayList();

    @Override
    public List<String> validate(Document doc) {
        log.debug("Início do teste Campo Invalido");

        errors = Lists.newArrayList();
        String tipoAtivo;


        //Capturando a identificação do ativo
        Identificador identifica = new Identificador();

        SecurityIdentification14 finInstrmId;
        List<AggregateBalanceInformation13> balForAcctList = doc.
                getSctiesBalAcctgRpt().getBalForAcct();

        //Verificar se a tag “SctiesBalAcctgRpt/BalForAcct” foi informada mais que uma vez
        if (balForAcctList.size() > 1) {
            errors.
                    add("Validação do Sistema Galgo: PA.0072 - Deve existir somente um Fundo "
                            + "ou Carteira por Informacao de Posição "
                            + "de Ativos. A sessão BalanceForAccount foi informada "
                            + balForAcctList.size() + " vezes. "
                            + "<BsnsMsg><SctiesBalAcctgRpt><BalForAcct>");
        }

        List<SubAccountIdentification16> subAcctDtlsLst = doc.getSctiesBalAcctgRpt().
                getSubAcctDtls();

        for (SubAccountIdentification16 subAcctDtls : subAcctDtlsLst) {
            List<AggregateBalanceInformation13> balForSubAcctLst = subAcctDtls.
                    getBalForSubAcct();

            // Percorrendo cada ativo
            for (AggregateBalanceInformation13 balForSubAcct : balForSubAcctLst) {

                //Verificando se a tag “FinInstrmId/OthrId/Tp/Prtry” ou “FinInstrmId/OthrId/Tp/Cd”
                //foi informada mais que uma vez com o mesmo tipo de identificação(Exemplo "SistemaGalgo")
                //vai retornar erro PA.0072
                List<String> listaIds = new ArrayList<>();
                //Utilizado Set pois não permite repetição de elementos
                Set<String> listaRepetidos = new HashSet<>();

                //Capturando a identificação do ativo
                finInstrmId = balForSubAcct.getFinInstrmId();
                String[] id = identifica.retornaIdentificador(finInstrmId);
                List<OtherIdentification1> othrIdLst = finInstrmId.getOthrId();

                for (OtherIdentification1 othrId : othrIdLst) {
                    tipoAtivo = othrId.getTp().getPrtry();
                    //Se tag <Prtry> tiver conteudo, verifica valor da variavel tipoAtivo
                    if (tipoAtivo != null) {
                        //Se já existe valor igual na lista, insere na lista de repetidos
                        if (listaIds.contains(tipoAtivo)) {
                            listaRepetidos.add(tipoAtivo);
                        } else {
                            listaIds.add(tipoAtivo);
                        }
                    } else {
                        //Se tag <Prtry> for nula, verifica a tag <Cd>
                        tipoAtivo = othrId.getTp().getCd();
                        if (listaIds.contains(tipoAtivo)) {
                            listaRepetidos.add(tipoAtivo);
                        } else {
                            listaIds.add(tipoAtivo);
                        }
                    }
                }

                //Se a lista de valores repetidos não está nula, então houve valores
                //repetidos e o aplicativo retorna erro
                if (!listaRepetidos.isEmpty()) {
                    for (String repetido : listaRepetidos) {

                        errors.add("Validação do Sistema Galgo: PA.0072 - Tipo de "
                                + "identificação " + repetido + " foi informado "
                                + "mais de uma vez na seção OtherIdentification, cada tipo de "
                                + "identificação pode ser informada no máximo uma vez. "
                                + "Verificar ativo de código " + id[1] + " igual a " + id[0] + ". "
                                + "<BalForAcct><FinInstrmId><OthrId>");
                    }
                }
            }

        }
        return errors;
    }
}
