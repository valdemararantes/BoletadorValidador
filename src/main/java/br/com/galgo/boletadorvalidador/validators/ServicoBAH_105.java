/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import com.google.common.collect.Lists;
import com.sistemagalgo.schemaposicaoativos.BsnsMsgComplexType;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import iso.std.iso._20022.tech.xsd.head_001_001.BusinessApplicationHeaderV01;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
//@GalgoValidatorType
//class ServicoBAH_105 implements Validator_ {
//
//    private static final Logger log = LoggerFactory.getLogger(
//            ServicoBAH_105.class);
//    private List<String> errors = Lists.newArrayList();
//
//    public List<String> validate( GalgoAssBalStmtComplexType doc) {
//        log.debug("Início do teste Serviço BAH");
//
//        errors = Lists.newArrayList();
//
//        List<BsnsMsgComplexType> bsnsMsgLst = doc.getBsnsMsg();
//        for(BsnsMsgComplexType listaMensagens : bsnsMsgLst){
//            String nmServco = listaMensagens.getAppHdr().getBizSvc();
//
//            if(!nmServco.equals("semt.003.001.04")){
//                log.debug("ALERTA SISTEMA GALGO - Para a tag <MsgDefIdr> é recomendado informar: "
//                        + "semt.003.001.04, foi informado "+nmServco);
//            }
//        }
//
//
//        return errors;
//}
//}
