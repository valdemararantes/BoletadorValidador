/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.validators;

import com.google.common.collect.Lists;
import iso.std.iso._20022.tech.xsd.head_001_001.BusinessApplicationHeaderV01;
import iso.std.iso._20022.tech.xsd.semt_003_001.Document;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author joyce.oliveira
 */
@GalgoValidatorType
class BAHTamanho_104 implements Validator_ {

    private static final Logger log = LoggerFactory.getLogger(
            BAHTamanho_104.class);
    private List<String> errors = Lists.newArrayList();


    public List<String> validate(Document doc) {
        log.debug("Início do teste Tamanho BAH");

        errors = Lists.newArrayList();
        int tamanhoLimiteBAH = 16000;

        BusinessApplicationHeaderV01 bah;
        bah = new BusinessApplicationHeaderV01();
        Class<? extends BusinessApplicationHeaderV01> conteudoBAH = bah.getClass();

        try {
            String stringBAH = bah.getSgntr().toString();

            if (stringBAH.length() > tamanhoLimiteBAH) {
                errors.add("O tamanho da assinatura, BAH, ultrapassa 16.000 "
                        + "caracteres, contém " + stringBAH.length()
                        + ". <GalgoAssBalStmt><BsnsMsg><AppHdr><Sgntr>");
            }
        } catch (NullPointerException e) {
        }
        return errors;
}
}
