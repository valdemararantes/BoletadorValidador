/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.reports;

import br.com.galgo.boletadorvalidador.validators.Validator;
import com.google.common.collect.Lists;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe que gera o arquivo de erros a partir da classe Validator, criando os cabeçalhos
 * das seções do arquivo de erros adequadamente, especificando qual o tipo de validação realizado
 * que originaram os erros.
 *
 * @author valdemar.arantes
 */
public class ValidatorErrorsHandler {

    private static final Logger log = LoggerFactory.getLogger(ValidatorErrorsHandler.class);
//    private final List<String> errors;
//    private final Validator.ErrorType errType;
//    private final Validator validator;
    private final File errOut;
    private final List<ValidatorErrors>validatorsErrors = Lists.newArrayList();
    private final List<String> allErrors = Lists.newArrayList();

    private static class ValidatorErrors {

        String title;
        List<String> errors;
        Validator.ErrorType errType;

        public ValidatorErrors(String title, List<String> errors, Validator.ErrorType errType) {
            this.title = title;
            this.errors = errors;
            this.errType = errType;
        }
    }

    public ValidatorErrorsHandler(File errOut) {
        this.errOut = errOut;
    }

    public ValidatorErrorsHandler(String title, List<String> errors, Validator.ErrorType errType,
            File errOut) {
        addValidateErrors(title, errors, errType);
        this.errOut = errOut;
    }

    /**
     * Adiciona à lista de erros de validadores.
     *
     * @param title
     * @param errors
     * @param errType
     */
    public final void addValidateErrors(String title, List<String> errors,
            Validator.ErrorType errType) {
        if (CollectionUtils.isEmpty(errors)) {
            return;
        }
        allErrors.addAll(errors);
        validatorsErrors.add(new ValidatorErrors(title, errors, errType));
    }

    /**
     * Gera o arquivo de erros com os cabeçalhos adequados dos tipos de erros encontrados.
     *
     * @throws IOException
     */
    public void printFile() throws IOException {
        if (CollectionUtils.isEmpty(allErrors)) {
            log.info("Lista de erros vazia. Retornando.");
            return;
        }

        ErrorReport errorReport = new ErrorReport(errOut);
        for (ValidatorErrors validatorErrors : validatorsErrors) {
            if (Validator.ErrorType.XSD.equals(validatorErrors.errType)) {
                errorReport.addErrors(
                        String.format("%s (%s)",
                                validatorErrors.title,
                                "Lista de erros de schema - XSD - encontrados"),
                        validatorErrors.errors);
            } else {
                errorReport.addErrors(
                        String.format("%s (%s)",
                                validatorErrors.title,
                                "Lista de erros ANBIMA/Galgo encontrados"),
                        validatorErrors.errors);
            }
        }
        errorReport.print();
    }
}
