/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.reports;

import java.util.List;

/**
 * Classe que contém os dados de uma seção do relatório de erros
 * @author valdemar.arantes
 */
class ErrorSection {
    private String title;
    public List<String> errors;

    public ErrorSection(String title,
            List<String> errors) {
        this.title = title;
        this.errors = errors;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getErrors() {
        return errors;
    }

}
