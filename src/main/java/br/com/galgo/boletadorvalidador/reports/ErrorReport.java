/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.reports;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.LoggerFactory;

/**
 * Classe responsável por imprimir o relatório de erros
 *
 * @author valdemar.arantes
 */
public class ErrorReport {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(ErrorReport.class);
    private File outFile;
    private List<ErrorSection> errorSections = new ArrayList<>();

    public void addErrors(String title, List<String> errors) {
        errorSections.add(new ErrorSection(title, errors));
    }

    /**
     *
     * @param outFile Arquivo de saída do relatório (txt)
     */
    public ErrorReport(File outFile) {
        this.outFile = outFile;
    }

    /**
     *
     * @return True se houver dados para o relatório
     */
    public boolean hasDataToPrint() {
        return !errorSections.isEmpty();
    }

    /**
     * Gera o arquivo com os erros
     */
    public void print() throws IOException {
        if (errorSections.isEmpty()) {
            log.info("Não existem dados para o relatório");
            return;
        }

        FileWriter fw = new FileWriter(outFile, false);
        int cont;
        
        for (ErrorSection errSect : errorSections) {
            fw.write(errSect.getTitle() + "\r\n\r\n");
            cont =0;
            for (String err : errSect.getErrors()) {
                cont++;
                fw.write(cont +". "+err + "\r\n");
            }
            fw.write("\n");
        }
        fw.close();
    }
}
