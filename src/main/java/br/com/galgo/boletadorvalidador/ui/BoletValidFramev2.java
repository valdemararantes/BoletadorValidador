/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.boletadorvalidador.ui;

import br.com.galgo.boletador.importers.WorkbookImporter;
import br.com.galgo.boletadorvalidador.reports.ValidatorErrorsHandler;
import br.com.galgo.boletadorvalidador.utils.Config;
import br.com.galgo.boletadorvalidador.utils.GalgoAssBalStmtUtils;
import br.com.galgo.boletadorvalidador.validators.Validator;
import br.com.galgo.boletadorvalidador.validators.ValidatorException;
import br.com.galgo.swingutils.FileChooserFactory;
import com.google.common.collect.Maps;
import com.sistemagalgo.schemaposicaoativos.GalgoAssBalStmtComplexType;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author JoyceCristina
 */
public class BoletValidFramev2 extends javax.swing.JFrame {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(BoletValidFramev2.class);
    private static final String VERSAO = Config.getString("app_versao");

    static {
        StringBuilder buff = new StringBuilder();
        buff.append("\n************************************************************************");
        buff.append("\n*******************   Início do Boletador_Validador   ******************");
        buff.append("\n*******************   Versão " + VERSAO + "     *********************************");
        buff.append("\n************************************************************************");
        log.info(buff.toString());
    }

    private javax.swing.JMenuItem GeneratetTemplateSheetMenuItem;
    //private javax.swing.JMenu ToolsMenu;
    //private javax.swing.JPanel abaBoletador;
    private javax.swing.JPanel abaValidador;
    private javax.swing.JButton adicionarXLSButton;
    private javax.swing.JButton adicionarXMLButton;
    //private javax.swing.JMenuItem ajudaMenuItem;
    private javax.swing.JPanel boletValidPanel;
    private javax.swing.JLabel jLabel1;
    //private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblVersion;
    private javax.swing.JTabbedPane painelAbas;
    private javax.swing.JPanel pnlHeader;
    private javax.swing.ButtonGroup radioGroupValidarANBIMA;
    private javax.swing.JRadioButton regrasANBIMARadioButton;
    private javax.swing.JRadioButton regrasGalgoRadioButton;
    //private javax.swing.JMenuItem sobreMenuItem;
    private javax.swing.JLabel textoBoletadorLabel;
    private javax.swing.JRadioButton umArquivoRadioButton;
    private javax.swing.JRadioButton variosArquivosRadioButton;
    /**
     * Creates new form BoletValidFramev2
     */
    public BoletValidFramev2() {
        initComponents();
        lblVersion.setText("v" + VERSAO);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        //        System.out.println( System.getProperty("log4j.configuration") );
        //        Enumeration<Object> keys = System.getProperties().keys();
        //
        //        while (keys.hasMoreElements()) {
        //            String key = (String) keys.nextElement();
        //            System.out.println(key + "=" + System.getProperty(key));
        //        }
        //        if (true) return;

        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel.
         * For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        //        try {
        //            for (javax.swing.UIManager.LookAndFeelInfo info
        //                    : javax.swing.UIManager.getInstalledLookAndFeels()) {
        //                if ("Nimbus".equals(info.getName())) {
        //                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
        //                    break;
        //                }
        //            }
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            log.error(null, e);
        }
        //</editor-fold>


        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BoletValidFramev2().setVisible(true);
            }
        });

    }

    @Override
    public List<Image> getIconImages() {
        List<Image> imageList = new ArrayList<Image>();
        imageList.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/1358907287_tick.png")));
        return imageList;
    }

    private void GeneratetTemplateSheetMenuItemActionPerformed(java.awt.event.ActionEvent evt)
    {//GEN-FIRST:event_GeneratetTemplateSheetMenuItemActionPerformed
        log.debug("Início...");

        JFileChooser modeloFileChooser = FileChooserFactory.newSaveTypedFile(null, "xls");
        int fileChooserOption = modeloFileChooser.showDialog(this, "Selecionar");
        log.debug("fileChooserOption={}", fileChooserOption);

        // Usuário não clicou em "Selecionar"
        if (fileChooserOption != JFileChooser.APPROVE_OPTION) {
            return;
        }

        File selectedFile = modeloFileChooser.getSelectedFile();
        InputStream templateInputStream = this.getClass().getClassLoader().getResourceAsStream("template.xls");
        try {
            int copiedBytes;
            try (OutputStream out = new FileOutputStream(selectedFile)) {
                copiedBytes = IOUtils.copy(templateInputStream, out);
                log.info("{} bytes copiados para {}", copiedBytes, selectedFile.getAbsolutePath());
                JOptionPane.showMessageDialog(this, "Arquivo salvo com sucesso.");
            }
        } catch (IOException e) {
            log.error(null, e);
            JOptionPane.showMessageDialog(this, "Erro ao salvar o arquivo.", "Erro", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            log.error(null, e);
            JOptionPane.showMessageDialog(this, "Erro no aplicativo.", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_GeneratetTemplateSheetMenuItemActionPerformed

    private void adicionarXLSButtonActionPerformed(java.awt.event.ActionEvent evt)
    {//GEN-FIRST:event_adicionarXLSButtonActionPerformed

        log.info("\n#####################################################" + "\n######## Botão Selecionar Planilha " +
                "acionado #########" + "\n#####################################################");

        //Abre caixa para selecionar planilhas que irão gerar XML
        final JFileChooser selecionaPlanilha = new JFileChooser();
        selecionaPlanilha.setDialogTitle("Selecione as planilhas para gerar o XML");
        selecionaPlanilha.setApproveButtonText("Gerar XML");
        FileFilter filter = new FileNameExtensionFilter("Planilhas Excel", "xlsx", "xls");
        selecionaPlanilha.setAcceptAllFileFilterUsed(false);
        selecionaPlanilha.setMultiSelectionEnabled(true);
        selecionaPlanilha.setFileFilter(filter);

        //Simular geração do arquivo, se usuário clicar em Gerar XML:
        if (selecionaPlanilha.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            SwingWorker<String, Void> worker;
            worker = new SwingWorker<String, Void>() {
                @Override
                protected String doInBackground() throws Exception {
                    try {
                        File selectedFile = selecionaPlanilha.getSelectedFile();
                        log.debug("Importando arquivo {}", selectedFile);

                        //Apresenta tela com mensagem "Gerando o XML"
                        ProgressBarDialog progBar = new ProgressBarDialog(BoletValidFramev2.this);
                        progBar.setTitle("Gerando o XML...");
                        progBar.setVisible(true);

                        InputStream xlsInputStream = new FileInputStream(selectedFile);
                        GalgoAssBalStmtComplexType glg = new WorkbookImporter(xlsInputStream).
                                importWorkbook();

                        progBar.setVisible(false);

                        //Caixa para Salvar XML gerado a partir de planilha(s)
                        JFileChooser salvaXML = new JFileChooser();
                        FileNameExtensionFilter filter = new FileNameExtensionFilter("Arquivos XML (*.xml)", "xml");
                        salvaXML.setFileFilter(filter);
                        salvaXML.showSaveDialog(salvaXML);

                        //Apresenta tela informando onde arquivo foi salvo
                        File arquivoSalvo = salvaXML.getSelectedFile();
                        if (arquivoSalvo == null) {
                            return "Fim sem salvar";
                        }

                        // Para transformar GalgoAssBalStmtComplexType em um XML correto, como
                        // a classe não tem a anotação XmlRootElement, preciso do trecho de
                        // código abaixo
                        //                        JAXBContext jc = JAXBContext.newInstance("org.example.customer");
                        //                        Marshaller marshaller = jc.createMarshaller();
                        //                        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                        //                        ObjectFactory objectFactory = new ObjectFactory();
                        //                        JAXBElement<GalgoAssBalStmtComplexType> galgoAssBalStmtElement =
                        // objectFactory.
                        //                                createGalgoAssBalStmt(glg);
                        //                        marshaller.marshal(galgoAssBalStmtElement, arquivoSalvo);
                        GalgoAssBalStmtUtils.marshall(glg, arquivoSalvo);

                        JFrame quadroSalvou = new JFrame();
                        JLabel mensagemCaminho = new JLabel(" Arquivo salvo no caminho: " + arquivoSalvo.
                                getAbsolutePath());
                        mensagemCaminho.setHorizontalAlignment(SwingConstants.CENTER);
                        quadroSalvou.setResizable(false);
                        quadroSalvou.add(mensagemCaminho);

                        quadroSalvou.setSize(700, 100);
                        quadroSalvou.setLocationRelativeTo(null);
                        quadroSalvou.setVisible(true);
                        return "Fim";

                    } catch (Exception e) {
                        log.warn(null, e);
                        return e.getMessage();
                    }
                }
            };
            worker.execute();
        }

    }//GEN-LAST:event_adicionarXLSButtonActionPerformed

    private void adicionarXMLButtonActionPerformed(java.awt.event.ActionEvent evt)
    {//GEN-FIRST:event_adicionarXMLButtonActionPerformed

        log.info("\n#####################################################" + "\n########### Botão Selecionar XML " +
                "acionado ###########" + "\n#####################################################");

        // Abre caixa para selecionar arquivo XML
        final JFileChooser selecionarXML = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Arquivos (*.xml)", "xml");
        selecionarXML.setDialogTitle("Selecione os arquivos XML para validar");
        selecionarXML.setApproveButtonText("Importar");
        selecionarXML.setAcceptAllFileFilterUsed(false);
        selecionarXML.setMultiSelectionEnabled(true);
        selecionarXML.setApproveButtonToolTipText("Selecionar arquivo(s) XML");
        selecionarXML.setFileFilter(filter);

        //Simula validação do arquivo, se usuário clicar em Salvar:
        if (selecionarXML.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            SwingWorker<String, Void> worker;
            worker = new SwingWorker<String, Void>() {
                @Override
                protected String doInBackground() throws Exception {
                    Map<String, Validator> errorsMap = Maps.newLinkedHashMap();
                    //Apresenta tela com mensagem "Validando XML"
                    ProgressBarDialog progBar = new ProgressBarDialog(BoletValidFramev2.this);
                    progBar.setTitle("Validando o XML...");
                    try {
                        progBar.setVisible(true);

                        // Validando
                        File[] selFiles = selecionarXML.getSelectedFiles();

                        //List<String> errors = Lists.newArrayList();
                        for (File selFile : selFiles) {
                            try {
                                log.debug("selFile={}", selFile.getCanonicalPath());

                                Validator validator = new Validator(selFile, regrasGalgoRadioButton.isSelected());
                                List<String> errors_ = validator.validate_();
                                if (!errors_.isEmpty()) {
                                    //errors.addAll(errors_);
                                    errorsMap.put(selFile.getName(), validator);
                                }
                            } catch (ExceptionInInitializerError e) {
                                log.error(null, e);
                                if (e.getCause() != null) {
                                    showErrMsg(e.getCause());
                                } else {
                                    showErrMsg(e);
                                }
                                throw new RuntimeException(e);
                            } catch (ValidatorException e) {
                                log.error(null, e);
                                showErrMsg(e);
                                throw new RuntimeException(e);
                            } catch (Throwable e) {
                                log.error(null, e);
                                showErrMsg(e);
                                //                                        errors.add(e.getMessage());
                                //                                        errorsMap.put(selFile.getName(), errors);
                                throw new RuntimeException(e);
                            }
                        }

                    } finally {
                        progBar.setVisible(false);
                    }

                    // XML não apresentou nenhum erro de validação
                    if (errorsMap.isEmpty()) {
                        log.info("Nao existem erros para se gerar um relatório");
                        JOptionPane.showMessageDialog(BoletValidFramev2.this, "Validação concluída com sucesso. " +
                                "Nenhum erro encontrado.");
                        log.debug("Saindo do SwingWorker");
                        return "Nao existem erros para se gerar um relatório";
                    }

                    //Caixa para Salvar arquivo TXT com validação
                    JFileChooser salvaTXT = new JFileChooser();
                    FileNameExtensionFilter filter = new FileNameExtensionFilter("Documentos de texto (*.txt)", "txt");
                    salvaTXT.setFileFilter(filter);
                    salvaTXT.showSaveDialog(salvaTXT);

                    File errOut = salvaTXT.getSelectedFile();
                    ValidatorErrorsHandler valErrHandler = new ValidatorErrorsHandler(errOut);

                    //ErrorReport errRpt = new ErrorReport(salvaTXT.getSelectedFile());
                    for (String section : errorsMap.keySet()) {
                        Validator validator = errorsMap.get(section);
                        valErrHandler.addValidateErrors(section, validator.getErrors(), validator.getErrorType());
                    }
                    //errRpt.print();
                    valErrHandler.printFile();

                    //Apresenta tela informando onde arquivo foi salvo
                    JOptionPane.showMessageDialog(BoletValidFramev2.this, "Arquivo \"" + errOut.getCanonicalPath() +
                            "\" salvo com sucesso");
                    return "Fim";
                }
            };
            worker.execute();
        }
    }//GEN-LAST:event_adicionarXMLButtonActionPerformed

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT
     * modify this code. The content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        radioGroupValidarANBIMA = new javax.swing.ButtonGroup();
        pnlHeader = new javax.swing.JPanel();
        boletValidPanel = new javax.swing.JPanel();
        painelAbas = new javax.swing.JTabbedPane();
        abaValidador = new javax.swing.JPanel();
        regrasANBIMARadioButton = new javax.swing.JRadioButton();
        regrasGalgoRadioButton = new javax.swing.JRadioButton();
        adicionarXMLButton = new javax.swing.JButton();
//        abaBoletador = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        textoBoletadorLabel = new javax.swing.JLabel();
        umArquivoRadioButton = new javax.swing.JRadioButton();
        variosArquivosRadioButton = new javax.swing.JRadioButton();
        adicionarXLSButton = new javax.swing.JButton();
        lblVersion = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
/*
        jMenuBar1 = new javax.swing.JMenuBar();
        ToolsMenu = new javax.swing.JMenu();
        GeneratetTemplateSheetMenuItem = new javax.swing.JMenuItem();
        JMenu ajudaMenu = new JMenu();
        ajudaMenuItem = new javax.swing.JMenuItem();
        sobreMenuItem = new javax.swing.JMenuItem();
*/

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Boletador/Validador " + VERSAO);
        setIconImages(getIconImages());
        setResizable(false);

        pnlHeader.setBackground(new java.awt.Color(20, 135, 216));

        radioGroupValidarANBIMA.add(regrasANBIMARadioButton);
        regrasANBIMARadioButton.setSelected(true);
        regrasANBIMARadioButton.setText("Validar apenas as regras ANBIMA");

        radioGroupValidarANBIMA.add(regrasGalgoRadioButton);
        regrasGalgoRadioButton.setText("Validar as regras ANBIMA assim como as do Sistema Galgo");
        regrasGalgoRadioButton.setToolTipText("Aciona a validação de regras do sistema Galgo");
        regrasGalgoRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                regrasGalgoRadioButtonActionPerformed(evt);
            }
        });

        adicionarXMLButton.setText("Selecionar XML...");
        adicionarXMLButton.setToolTipText("Selecionar arquivo(s) XML para validação");
        adicionarXMLButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adicionarXMLButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout abaValidadorLayout = new javax.swing.GroupLayout(abaValidador);
        abaValidador.setLayout(abaValidadorLayout);
        abaValidadorLayout.setHorizontalGroup(abaValidadorLayout.createParallelGroup(javax.swing.GroupLayout
                .Alignment.LEADING).addGroup(abaValidadorLayout.createSequentialGroup().addContainerGap().addGroup
                (abaValidadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup
                        (abaValidadorLayout.createSequentialGroup().addComponent(regrasANBIMARadioButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 118, javax.swing
                                        .GroupLayout.PREFERRED_SIZE)).addGroup(abaValidadorLayout.createParallelGroup
                        (javax.swing.GroupLayout.Alignment.LEADING).addComponent(adicionarXMLButton).addComponent
                        (regrasGalgoRadioButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout
                                .DEFAULT_SIZE, Short.MAX_VALUE))).addGap(235, 235, 235)));
        abaValidadorLayout.setVerticalGroup(abaValidadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment
                .LEADING).addGroup(abaValidadorLayout.createSequentialGroup().addGap(21, 21, 21).addComponent
                (regrasANBIMARadioButton).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(regrasGalgoRadioButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout
                        .DEFAULT_SIZE, Short.MAX_VALUE).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement
                        .RELATED).addComponent(adicionarXMLButton).addGap(41, 41, 41)));

        painelAbas.addTab("Validador", abaValidador);

        textoBoletadorLabel.setText("Se selecionar várias planilhas, escolha uma das opções abaixo:");
        textoBoletadorLabel.setPreferredSize(new java.awt.Dimension(331, 50));

        radioGroupValidarANBIMA.add(umArquivoRadioButton);
        umArquivoRadioButton.setText("Gerar um único XML para todas as planilhas");
        umArquivoRadioButton.setToolTipText("Será gerado apenas um arquivo XML a partir de todas plailhas " +
                "selecionadas");
        umArquivoRadioButton.setAlignmentX(0.5F);
        umArquivoRadioButton.setAlignmentY(2.5F);
        umArquivoRadioButton.setDisplayedMnemonicIndex(1);
        umArquivoRadioButton.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        radioGroupValidarANBIMA.add(variosArquivosRadioButton);
        variosArquivosRadioButton.setText("Gerar um XML para cada planilha");
        variosArquivosRadioButton.setToolTipText("Será gerado um arquivo XL para cada planilha selecionada");
        variosArquivosRadioButton.setDisplayedMnemonicIndex(2);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 0, Short.MAX_VALUE).addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout
                        .Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup
                        (jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent
                                (textoBoletadorLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 524, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createSequentialGroup().addGroup(jPanel1Layout
                                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent
                                                (variosArquivosRadioButton).addComponent(umArquivoRadioButton))
                                        .addGap(0, 0, Short.MAX_VALUE))).addContainerGap())));
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 0, Short.MAX_VALUE).addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout
                        .Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addGap(12, 12, 12)
                        .addComponent(textoBoletadorLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 14, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent
                                (umArquivoRadioButton).addGap(8, 8, 8).addComponent(variosArquivosRadioButton).addGap
                                (12, 12, 12))));

        adicionarXLSButton.setToolTipText("Selecionar planilha(s) para gerar XML");
        adicionarXLSButton.setText("Selecionar planilha...");
        adicionarXLSButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adicionarXLSButtonActionPerformed(evt);
            }
        });

/*
        javax.swing.GroupLayout abaBoletadorLayout = new javax.swing.GroupLayout(abaBoletador);
        abaBoletador.setLayout(abaBoletadorLayout);
        abaBoletadorLayout.setHorizontalGroup(abaBoletadorLayout.createParallelGroup(javax.swing.GroupLayout
                .Alignment.LEADING).addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing
                .GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addGroup
                (abaBoletadorLayout.createSequentialGroup().addContainerGap().addComponent(adicionarXLSButton)
                        .addContainerGap(405, Short.MAX_VALUE)));
        abaBoletadorLayout.setVerticalGroup(abaBoletadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment
                .LEADING).addGroup(abaBoletadorLayout.createSequentialGroup().addComponent(jPanel1, javax.swing
                .GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing
                .LayoutStyle.ComponentPlacement.RELATED).addComponent(adicionarXLSButton).addContainerGap(67, Short
                .MAX_VALUE)));

        painelAbas.addTab("Boletador", abaBoletador);
*/

        lblVersion.setText("VERSÃO");

        javax.swing.GroupLayout boletValidPanelLayout = new javax.swing.GroupLayout(boletValidPanel);
        boletValidPanel.setLayout(boletValidPanelLayout);
        boletValidPanelLayout.setHorizontalGroup(boletValidPanelLayout.createParallelGroup(javax.swing.GroupLayout
                .Alignment.LEADING).addComponent(painelAbas).addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                boletValidPanelLayout.createSequentialGroup().addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE,
                        Short.MAX_VALUE).addComponent(lblVersion).addContainerGap()));
        boletValidPanelLayout.setVerticalGroup(boletValidPanelLayout.createParallelGroup(javax.swing.GroupLayout
                .Alignment.LEADING).addGroup(boletValidPanelLayout.createSequentialGroup().addContainerGap(javax
                .swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addComponent(painelAbas, javax.swing.GroupLayout
                .PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE).addPreferredGap(javax.swing.LayoutStyle
                .ComponentPlacement.RELATED).addComponent(lblVersion).addGap(43, 43, 43)));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/galgo_banner.png"))); // NOI18N
        jLabel1.setText("trtrtrtrtrtrt");

        javax.swing.GroupLayout pnlHeaderLayout = new javax.swing.GroupLayout(pnlHeader);
        pnlHeader.setLayout(pnlHeaderLayout);
        pnlHeaderLayout.setHorizontalGroup(pnlHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment
                .LEADING).addComponent(boletValidPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing
                .GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addGroup
                (pnlHeaderLayout.createSequentialGroup().addContainerGap().addComponent(jLabel1, javax.swing
                        .GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE).addContainerGap
                        (javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
        pnlHeaderLayout.setVerticalGroup(pnlHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment
                .LEADING).addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlHeaderLayout.createSequentialGroup
                ().addContainerGap().addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, Short
                .MAX_VALUE).addGap(18, 18, 18).addComponent(boletValidPanel, javax.swing.GroupLayout.PREFERRED_SIZE,
                176, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)));

/*
        ToolsMenu.setText("Ferramentas");
        ToolsMenu.setToolTipText("");

        GeneratetTemplateSheetMenuItem.setText("Gerar Planilha Modelo");
        GeneratetTemplateSheetMenuItem.setToolTipText("");
        GeneratetTemplateSheetMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GeneratetTemplateSheetMenuItemActionPerformed(evt);
            }
        });
        ToolsMenu.add(GeneratetTemplateSheetMenuItem);

        jMenuBar1.add(ToolsMenu);

        ajudaMenu.setText("Ajuda");

        ajudaMenuItem.setText("Ajuda");
        ajudaMenuItem.setEnabled(false);
        ajudaMenu.add(ajudaMenuItem);

        sobreMenuItem.setText("Sobre ...");
        sobreMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sobreMenuItemActionPerformed(evt);
            }
        });
        ajudaMenu.add(sobreMenuItem);

        jMenuBar1.add(ajudaMenu);

        setJMenuBar(jMenuBar1);
*/

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent
                (pnlHeader, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax
                        .swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent
                (pnlHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing
                        .GroupLayout.PREFERRED_SIZE));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void regrasGalgoRadioButtonActionPerformed(java.awt.event.ActionEvent evt)
    {//GEN-FIRST:event_regrasGalgoRadioButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_regrasGalgoRadioButtonActionPerformed

    private void showErrMsg(ValidatorException e) {
        JOptionPane.showMessageDialog(BoletValidFramev2.this, "Ocorreu um erro no aplicativo:\n[" + e.getMessage() +
                "]\n" + "Por favor, entre em contato com o suporte.", "Erro", JOptionPane.ERROR_MESSAGE);
    }

    private void showErrMsg(Throwable e) {
        JOptionPane.showMessageDialog(BoletValidFramev2.this, "Ocorreu um erro no aplicativo:\n[" + e.getMessage() +
                "]\n" + "Por favor, entre em contato com o suporte.", "Erro", JOptionPane.ERROR_MESSAGE);
    }

    private void sobreMenuItemActionPerformed(java.awt.event.ActionEvent evt)
    {//GEN-FIRST:event_sobreMenuItemActionPerformed
        //PopUp com informações do Boletador/Validador
        JFrame quadroSobre = new JFrame();
        JLabel txtSobre = new JLabel("Boletador/Validador v" + VERSAO);
        quadroSobre.add(txtSobre);
        txtSobre.setHorizontalAlignment(SwingConstants.CENTER);
        quadroSobre.setSize(300, 200);
        quadroSobre.setLocationRelativeTo(null);
        quadroSobre.setVisible(true);
    }//GEN-LAST:event_sobreMenuItemActionPerformed
    // End of variables declaration//GEN-END:variables
}
